//go:build linux

package main

import (
	"fmt"
	"gitee.com/sy_183/go-common/assert"
	"gitee.com/sy_183/go-common/uns"
	"net"
	"syscall"
	"unsafe"
)

func main() {
	conn, err := net.ListenUDP("udp", assert.Must(net.ResolveUDPAddr("udp", "0.0.0.0:5004")))
	if err != nil {
		panic(err)
	}
	assert.Must(conn.SyscallConn()).Control(func(fd uintptr) {
		syscall.SetsockoptInt(int(fd), syscall.IPPROTO_IP, syscall.IP_PKTINFO, 1)
		syscall.SetsockoptInt(int(fd), syscall.IPPROTO_IP, syscall.IP_RECVTTL, 1)
	})
	defer conn.Close()
	buf := make([]byte, 2048)
	oob := make([]byte, 2048)
	for {
		n, oobn, flags, addr, err := conn.ReadMsgUDP(buf, oob)
		if err != nil {
			panic(err)
		}
		fmt.Printf("receive from %s %d bytes\n", addr, n)
		fmt.Printf("flags: %d, obb data: %v\n", flags, oob[:oobn])
		if oobn > 0 {
			msg := uns.FromPointer[byte, syscall.Cmsghdr](&oob[0])
			oob = oob[unsafe.Sizeof(syscall.Cmsghdr{}):]
			if msg.Level == syscall.IPPROTO_IP && msg.Type == syscall.IP_PKTINFO {
				packetInfo := uns.FromPointer[byte, syscall.Inet4Pktinfo](&oob[0])
				fmt.Println(packetInfo)
			}
		}
		fmt.Println(uns.BytesToString(buf[:n]))
	}
}
