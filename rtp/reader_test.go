package rtp

import (
	"bufio"
	"fmt"
	"gitee.com/sy_183/go-common/unit"
	"io"
	"os"
	"testing"
)

func TestReader(t *testing.T) {
	fp, err := os.Open("C:\\Users\\suy\\Documents\\Language\\Go\\rtp\\tools\\tcp-dump\\192.168.1.108-9720.tcp")
	if err != nil {
		t.Fatal(err)
	}
	defer fp.Close()
	r := Reader{Reader: bufio.NewReaderSize(fp, unit.MeBiByte)}
	for {
		layer, err := r.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			t.Fatal(err)
		}
		fmt.Println(layer)
	}
}

type BB struct {
	A string
	B string
	C string
}

func (b *BB) String() string {
	return fmt.Sprintf("%s %s %s", b.A, b.B, b.C)
}

func GetBB() string {
	var a string
	var b string
	var c string
	_ = a
	_ = b
	_ = c
	return (&BB{
		A: "1",
		B: "2",
		C: "3",
	}).String()
}

func TestBB(t *testing.T) {
	GetBB()
}
