package rtp

import (
	"encoding/binary"
	"gitee.com/sy_183/go-common/parser"
	"gitee.com/sy_183/go-common/slice"
)

type InputLayerParser struct {
	layer        *InputLayer
	size         int
	minSize      int
	csrcLen      uint8
	extensionLen uint16
}

func (p *InputLayerParser) Init(layer *InputLayer, data []byte) error {
	p.layer = layer
	p.minSize = BaseHeaderSize
	p.size = len(data)
	if p.size < p.minSize {
		return RtpPacketError{ErrInvalidPacketSize}
	}
	return nil
}

func (p *InputLayerParser) InitChunks(layer *InputLayer, chunks [][]byte) error {
	p.layer = layer
	p.minSize = BaseHeaderSize
	for _, chunk := range chunks {
		p.size += len(chunk)
	}
	if p.size < p.minSize {
		return RtpPacketError{ErrInvalidPacketSize}
	}
	return nil
}

func (p *InputLayerParser) parseBaseHeader(data []byte) error {
	l := p.layer
	l.first = data[0]
	l.second = data[1]
	if l.Version() != 2 {
		return RtpPacketError{ErrInvalidVersion}
	}
	l.sequenceNumber = binary.BigEndian.Uint16(data[2:4])
	l.timestamp = binary.BigEndian.Uint32(data[4:8])
	l.ssrc = binary.BigEndian.Uint32(data[8:12])
	p.csrcLen = l.first & 0b00001111

	// 计算RTP包最小大小
	p.minSize += int(p.csrcLen) * 4
	if l.HasExtension() {
		p.minSize += 4
	}
	if l.HasPadding() {
		p.minSize += 1
	}
	if p.size < p.minSize {
		return RtpPacketError{ErrInvalidPacketSize}
	}
	return nil
}

func (p *InputLayerParser) parseCSRC(data []byte) {
	p.layer.csrcs = slice.AssignLen(p.layer.csrcs[:0], int(p.csrcLen))
	for i := uint8(0); i < p.csrcLen; i++ {
		p.layer.csrcs[i] = binary.BigEndian.Uint32(data[:4])
		data = data[4:]
	}
}

func (p *InputLayerParser) parseExtensionHeader(data []byte) error {
	p.layer.extension.profile = binary.BigEndian.Uint16(data[:2])
	p.extensionLen = binary.BigEndian.Uint16(data[2:4])

	// 计算RTP包最小大小
	p.minSize += int(p.extensionLen) * 4
	if p.size < p.minSize {
		return RtpPacketError{ErrInvalidPacketSize}
	}
	return nil
}

func (p *InputLayerParser) parseExtensionData(data []byte) {
	extension := &p.layer.extension
	extension.contents = slice.AssignLen(extension.contents[:0], int(p.extensionLen))
	for i := uint16(0); i < p.extensionLen; i++ {
		extension.contents[i] = binary.BigEndian.Uint32(data[:4])
		data = data[4:]
	}
}

func (p *InputLayerParser) checkPadding() error {
	padding := p.layer.padding
	if padding == 0 {
		return RtpPacketError{ErrInvalidPaddingLength}
	}
	p.minSize += int(padding - 1)
	if p.size < p.minSize {
		return RtpPacketError{ErrInvalidPacketSize}
	}
	return nil
}

func (p *InputLayerParser) Parse(data []byte) error {
	baseHeader, cur := data[:BaseHeaderSize], data[BaseHeaderSize:]
	if err := p.parseBaseHeader(baseHeader); err != nil {
		return err
	}

	// parse csrc list
	csrcData, cur := cur[:p.csrcLen*4], cur[p.csrcLen*4:]
	p.parseCSRC(csrcData)

	// parse extension if exist
	if p.layer.HasExtension() {
		// parse extension header
		var extensionHeader, extensionData []byte
		extensionHeader, cur = cur[:4], cur[4:]
		if err := p.parseExtensionHeader(extensionHeader); err != nil {
			return err
		}

		// parse extension content
		extensionData, cur = cur[:p.extensionLen*4], cur[p.extensionLen*4:]
		p.parseExtensionData(extensionData)
	}

	// parse padding
	if p.layer.HasPadding() {
		p.layer.padding = Padding(cur[len(cur)-1])
		if err := p.checkPadding(); err != nil {
			return err
		}
	}

	// parse payload content
	p.layer.payload.Clear()
	p.layer.payload.Append(cur[:p.size-p.minSize])
	return nil
}

func shiftChunk(chunks [][]byte, offset int) ([]byte, [][]byte) {
	chunk := chunks[0]
	if offset < len(chunk) {
		chunks[0] = chunk[offset:]
		chunk = chunk[:offset]
	} else {
		chunks = chunks[1:]
	}
	return chunk, chunks
}

func readChunks(chunks [][]byte, size int) ([]byte, [][]byte) {
	var bs []byte
	for len(bs) < size {
		var chunk []byte
		chunk, chunks = shiftChunk(chunks, size-len(bs))
		if bs == nil {
			bs = chunk
		} else {
			bs = append(bs, chunk...)
		}
	}
	return bs, chunks
}

func (p *InputLayerParser) ParseChunks(chunks [][]byte) error {
	baseHeader, chunks := readChunks(chunks, BaseHeaderSize)
	if err := p.parseBaseHeader(baseHeader); err != nil {
		return err
	}

	// parse csrc list
	csrcDataSize := int(p.csrcLen * 4)
	csrcData, chunks := readChunks(chunks, csrcDataSize)
	p.parseCSRC(csrcData)

	if p.layer.HasExtension() {
		// parse extension header
		var extensionHeader, extensionData []byte
		extensionHeader, chunks = readChunks(chunks, 4)
		if err := p.parseExtensionHeader(extensionHeader); err != nil {
			return err
		}

		// parse extension content
		extensionDataSize := int(p.extensionLen * 4)
		extensionData, chunks = readChunks(chunks, extensionDataSize)
		p.parseExtensionData(extensionData)
	}

	// parse padding
	if p.layer.HasPadding() {
		lastChunk := chunks[len(chunks)-1]
		paddingLength := lastChunk[len(lastChunk)-1]
		p.layer.padding = Padding(paddingLength)
		if err := p.checkPadding(); err != nil {
			return err
		}
	}

	// parse payload content
	if payload, payloadSize := &p.layer.payload, p.size-p.minSize; payloadSize > 0 {
		var chunk []byte
		for payload.size < payloadSize {
			chunk, chunks = shiftChunk(chunks, payloadSize-payload.size)
			payload.Append(chunk)
		}
	} else {
		payload.Clear()
	}

	return nil
}

type StreamParser struct {
	Layer      *InputLayer
	Length     uint16
	needParser parser.NeedParser
	parseBody  bool
}

func (p *StreamParser) Reset() {
	p.needParser.Reset()
	p.parseBody = false
}

func (p *StreamParser) Parse(buf []byte) (ok bool, remain []byte, err error) {
	remain = buf
	for len(remain) > 0 {
		if p.needParser.ParseP(remain, &remain) {
			if !p.parseBody {
				if p.needParser.Need() == 0 {
					p.needParser.SetNeed(2)
				}
				header := p.needParser.Merge()
				p.Length = binary.BigEndian.Uint16(header)
				p.needParser.SetNeed(int(p.Length))
				p.parseBody = true
			} else {
				if err = p.Layer.UnmarshalChunks(p.needParser.Get()); err == nil {
					ok = true
				}
				p.needParser.SetNeed(2)
				p.parseBody = false
				return
			}
		}
	}
	return
}

type RTSPParser struct {
	Layer       *InputLayer
	Interleaved uint8
	Length      uint16
	needParser  parser.NeedParser
	parseBody   bool
}

func (p *RTSPParser) Reset() {
	p.needParser.Reset()
	p.parseBody = false
}

func (p *RTSPParser) Parse(buf []byte) (ok bool, remain []byte, err error) {
	remain = buf
	for len(remain) > 0 {
		if p.needParser.ParseP(remain, &remain) {
			if !p.parseBody {
				if p.needParser.Need() == 0 {
					p.needParser.SetNeed(4)
				}
				header := p.needParser.Merge()
				if header[0] != '$' {
					err = ErrInvalidRTSPPrefix
					return
				}
				p.Interleaved = header[1]
				p.Length = binary.BigEndian.Uint16(header[2:])
				p.needParser.SetNeed(int(p.Length))
				p.parseBody = true
			} else {
				if err = p.Layer.UnmarshalChunks(p.needParser.Get()); err == nil {
					ok = true
				}
				p.needParser.SetNeed(4)
				p.parseBody = false
				return
			}
		}
	}
	return
}
