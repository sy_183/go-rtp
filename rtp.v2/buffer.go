package rtp

import (
	"gitee.com/sy_183/go-common/unit"
)

const (
	DefaultBufferPoolSize = unit.KiBiByte * 256
	DefaultBufferReverse  = 2048
)
