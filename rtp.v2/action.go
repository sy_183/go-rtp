package rtp

type Action int

const (
	ActionListen = Action(1 << iota)
	ActionDial
	ActionAccept
	ActionRead
	ActionSetDeadline
	ActionSetReadDeadline
	ActionClose
	ActionTimeout
	ActionAllocBuf
	ActionNewChannel
)

type (
	ChannelActionHandler = func(channel Channel, action Action, err error, args ...any)
	ServerActionHandler  = func(server Server, action Action, err error, args ...any)
)
