package rtp

import (
	"net"
	"sync/atomic"
	"time"
)

type UDPStream struct {
	server *UDPServer

	localAddr  *net.UDPAddr
	remoteAddr atomic.Pointer[net.UDPAddr]
	ssrc       atomic.Int64
	timeout    time.Duration

	lastTime time.Time

	isInit bool
	seq    uint16

	streamHandler
	ctx any
}

func (s *UDPStream) setTimeout(timeout time.Duration) {
	s.timeout = timeout
}

func (s *UDPStream) setContext(ctx any) {
	s.ctx = ctx
}

func (s *UDPStream) deadline() time.Time {
	if s.timeout > 0 {
		return s.lastTime.Add(s.timeout)
	}
	return zeroTime
}

func (s *UDPStream) doHandlePacket(packet *InputPacket) error {
	if s.keepChooser == nil {
		s.keepChooser = NewKeepChooser(0, 0, nil)
	}
	s.keepChooser.OnSuccess()

	uAddr, is := packet.Addr().(*net.UDPAddr)
	if !is {
		packet.Release()
		return ErrPacketDropped
	}

	var rAddr *net.UDPAddr
	if !s.remoteAddr.CompareAndSwap(nil, uAddr) {
		rAddr = s.remoteAddr.Load()
		if !rAddr.IP.Equal(uAddr.IP) ||
			rAddr.Port != uAddr.Port ||
			rAddr.Zone != uAddr.Zone {
			// packet addr not matched, dropped
			packet.Release()
			return ErrPacketDropped
		}
	}
	rSsrc := packet.SSRC()
	if !s.ssrc.CompareAndSwap(-1, int64(rSsrc)) {
		ssrc := s.ssrc.Load()
		if ssrc != int64(rSsrc) {
			packet.Release()
			return ErrPacketDropped
		}
	}

	// count rtp packet loss
	if !s.isInit {
		s.isInit = true
	} else {
		if diff := packet.SequenceNumber() - s.seq; diff != 1 {
			s.doOnLossPacket(int(diff) - 1)
		}
	}
	s.seq = packet.SequenceNumber()
	if s.packetHandler != nil {
		return s.packetHandler(s, packet)
	}
	packet.Release()
	return nil
}

func (s *UDPStream) doOnParseError(err error) error {
	if s.onParseError != nil {
		if err := s.onParseError(s, err); err != nil {
			return err
		}
	}
	if s.keepChooser == nil {
		s.keepChooser = NewDefaultKeepChooser()
	}
	if !s.keepChooser.OnError(err) {
		return ErrTooManyInvalidPacket
	}
	return nil
}

func (s *UDPStream) doOnLossPacket(loss int) {
	if s.onLossPacket != nil {
		s.onLossPacket(s, loss)
	}
}

func (s *UDPStream) doOnTimeout() {
	if s.onTimeout != nil {
		s.onTimeout(s)
	}
}

func (s *UDPStream) doOnClose() {
	if s.onClose != nil {
		s.onClose(s)
	}
}

func (s *UDPStream) SSRC() int64 {
	return s.ssrc.Load()
}

func (s *UDPStream) SetSSRC(ssrc int64) Stream {
	s.ssrc.Store(ssrc)
	return s
}

func (s *UDPStream) LocalAddr() net.Addr {
	return s.localAddr
}

func (s *UDPStream) RemoteAddr() net.Addr {
	return s.remoteAddr.Load()
}

func (s *UDPStream) SetRemoteAddr(addr net.Addr) Stream {
	s.remoteAddr.Store(addr.(*net.UDPAddr))
	return s
}

func (s *UDPStream) Context() any {
	return s.ctx
}

func (s *UDPStream) Close() error {
	return s.server.Close()
}
