package rtp

import (
	"net"
)

type Stream interface {
	SSRC() int64

	SetSSRC(ssrc int64) Stream

	LocalAddr() net.Addr

	RemoteAddr() net.Addr

	SetRemoteAddr(addr net.Addr) Stream

	Context() any

	Close() error
}

type (
	PacketHandler        = func(stream Stream, packet *InputPacket) error
	ParseErrorHandler    = func(stream Stream, err error) error
	LossPacketHandler    = func(stream Stream, loss int)
	StreamTimeoutHandler = func(stream Stream)
	StreamCloseHandler   = func(stream Stream)
)

type streamHandler struct {
	packetHandler PacketHandler
	keepChooser   KeepChooser
	onParseError  ParseErrorHandler
	onLossPacket  LossPacketHandler
	onTimeout     StreamTimeoutHandler
	onClose       StreamCloseHandler
}

func (h *streamHandler) setPacketHandler(handler PacketHandler) {
	h.packetHandler = handler
}

func (h *streamHandler) setKeepChooser(chooser KeepChooser) {
	h.keepChooser = chooser
}

func (h *streamHandler) setOnParseError(handler ParseErrorHandler) {
	h.onParseError = handler
}

func (h *streamHandler) setOnLossPacket(handler LossPacketHandler) {
	h.onLossPacket = handler
}

func (h *streamHandler) setOnTimeout(handler StreamTimeoutHandler) {
	h.onTimeout = handler
}

func (h *streamHandler) setOnClose(handler StreamCloseHandler) {
	h.onClose = handler
}
