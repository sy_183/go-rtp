package rtp

import (
	"encoding/binary"
	"fmt"
	ioUtil "gitee.com/sy_183/go-common/utils/io"
	"io"
	"math"
	"strconv"
	"strings"
)

const (
	BaseHeaderSize = 12
	MaxCSRCs       = 15
	MaxExtensions  = math.MaxUint16
)

type PayloadI interface {
	Size() int
	Bytes() []byte
	io.WriterTo
	Clear()
}

type HeaderI interface {
	Version() uint8
	HasPadding() bool
	HasExtension() bool
	SetExtension(extension bool)
	Marker() bool
	SetMarker(maker bool)
	PayloadType() uint8
	SetPayloadType(payloadType uint8)
	SequenceNumber() uint16
	SetSequenceNumber(seq uint16)
	Timestamp() uint32
	SetTimestamp(timestamp uint32)
	SSRC() uint32
	SetSSRC(ssrc uint32)
	CSRCs() *CSRCs
	Extension() *Extension
	io.WriterTo
	Clear()
}

// LayerI 接口为RTP报文的结构化接口，通过此接口可以获取或修改RTP报文的结构
type LayerI interface {
	HeaderI
	Payload() PayloadI
	PaddingLength() uint8
	SetPaddingLength(length uint8)
	Size() int
	Bytes() []byte
	fmt.Stringer
}

type CSRCs []uint32

func (cs *CSRCs) Len() int {
	return len(*cs)
}

func (cs *CSRCs) Size() int {
	return len(*cs) * 4
}

func (cs *CSRCs) Get(i int) uint32 {
	return (*cs)[i]
}

func (cs *CSRCs) Set(i int, csrc uint32) {
	(*cs)[i] = csrc
}

func (cs *CSRCs) Append(csrcs ...uint32) {
	if limit := MaxCSRCs - len(*cs); len(csrcs) > limit {
		csrcs = csrcs[:limit]
	}
	*cs = append(*cs, csrcs...)
}

func (cs *CSRCs) Replace(csrcs ...uint32) {
	if len(csrcs) > MaxCSRCs {
		csrcs = csrcs[:MaxCSRCs]
	}
	*cs = append((*cs)[:0], csrcs...)
}

func (cs *CSRCs) Clear() {
	*cs = (*cs)[:0]
}

type Extension struct {
	profile  uint16
	contents []uint32
}

func (e *Extension) Profile() uint16 {
	return e.profile
}

func (e *Extension) SetProfile(profile uint16) {
	e.profile = profile
}

func (e *Extension) Contents() []uint32 {
	return e.contents
}

func (e *Extension) Len() int {
	return len(e.contents)
}

func (e *Extension) Size() int {
	return len(e.contents) * 4
}

func (e *Extension) Get(i int) uint32 {
	return e.contents[i]
}

func (e *Extension) Set(i int, content uint32) {
	e.contents[i] = content
}

func (e *Extension) Append(contents ...uint32) {
	if limit := MaxExtensions - len(e.contents); len(contents) > limit {
		contents = contents[:limit]
	}
	e.contents = append(e.contents, contents...)
}

func (e *Extension) Replace(contents ...uint32) {
	if len(contents) > MaxExtensions {
		contents = contents[:MaxExtensions]
	}
	e.contents = append(e.contents[:0], contents...)
}

func (e *Extension) Clear() {
	e.profile = 0
	e.contents = e.contents[:0]
}

type Padding uint8

const paddingBufLen = 16

func (p Padding) Len() uint8 {
	return uint8(p)
}

func (p Padding) WriteTo(w io.Writer) (n int64, err error) {
	var buf [paddingBufLen]byte
	for remain := uint8(p); true; remain -= uint8(paddingBufLen) {
		if remain <= uint8(paddingBufLen) {
			buf[remain-1] = uint8(p)
			if err = ioUtil.Write(w, buf[:remain], &n); err != nil {
				return
			}
			break
		}
		if err = ioUtil.Write(w, buf[:], &n); err != nil {
			return
		}
	}
	return
}

type Header struct {
	first          uint8
	second         uint8
	sequenceNumber uint16
	timestamp      uint32
	ssrc           uint32
	csrcs          CSRCs
	extension      Extension
}

func MakeHeader() Header {
	return Header{first: 2 << 6}
}

func (h *Header) Version() uint8 {
	return h.first >> 6
}

func (h *Header) HasPadding() bool {
	return h.first&0b00100000 != 0
}

func (h *Header) setPadding(padding bool) {
	if padding {
		h.first |= 0b00100000
	} else {
		h.first &= ^uint8(0b00100000)
	}
}

func (h *Header) HasExtension() bool {
	return h.first&0b00010000 != 0
}

func (h *Header) SetExtension(extension bool) {
	if extension {
		h.first |= 0b00010000
	} else {
		h.first &= ^uint8(0b00010000)
	}
}

func (h *Header) Marker() bool {
	return h.second&0b10000000 != 0
}

func (h *Header) SetMarker(maker bool) {
	if maker {
		h.second |= 0b10000000
	} else {
		h.second &= ^uint8(0b10000000)
	}
}

func (h *Header) PayloadType() uint8 {
	return h.second & 0b01111111
}

func (h *Header) SetPayloadType(payloadType uint8) {
	h.second |= payloadType & 0b01111111
}

func (h *Header) SequenceNumber() uint16 {
	return h.sequenceNumber
}

func (h *Header) SetSequenceNumber(seq uint16) {
	h.sequenceNumber = seq
}

func (h *Header) Timestamp() uint32 {
	return h.timestamp
}

func (h *Header) SetTimestamp(timestamp uint32) {
	h.timestamp = timestamp
}

func (h *Header) SSRC() uint32 {
	return h.ssrc
}

func (h *Header) SetSSRC(ssrc uint32) {
	h.ssrc = ssrc
}

func (h *Header) CSRCs() *CSRCs {
	return &h.csrcs
}

func (h *Header) Extension() *Extension {
	return &h.extension
}

func (h *Header) WriteTo(w io.Writer) (n int64, err error) {
	var buf [paddingBufLen]byte
	writer := ioUtil.Writer{Buf: buf[:]}

	defer func() { err = ioUtil.HandleRecovery(recover()) }()

	// write base header
	buf[0], buf[1] = (uint8(len(h.csrcs))&0b00001111)|(h.first&0b11110000), h.second
	binary.BigEndian.PutUint16(buf[2:], h.sequenceNumber)
	binary.BigEndian.PutUint32(buf[4:], h.timestamp)
	binary.BigEndian.PutUint32(buf[8:], h.ssrc)
	ioUtil.WritePanic(w, buf[:12], &n)

	// write csrc list
	if len(h.csrcs) > 0 {
		for _, csrc := range h.csrcs {
			if writer.WriteUint32(csrc); writer.Len() == paddingBufLen {
				ioUtil.WriteAndResetPanic(&writer, w, &n)
			}
		}
		ioUtil.WriteAndResetPanic(&writer, w, &n)
	}

	// write extension if necessary
	if h.HasExtension() {
		// write extension header
		extension := &h.extension
		extensionLength := extension.Len()
		writer.WriteUint16(extension.profile).WriteUint16(uint16(extensionLength))

		// write extension content
		for i := 0; i < extensionLength; i++ {
			if writer.WriteUint32(extension.contents[i]); writer.Len() == paddingBufLen {
				ioUtil.WriteAndResetPanic(&writer, w, &n)
			}
		}
		ioUtil.WriteAndResetPanic(&writer, w, &n)
	}

	return
}

func (h *Header) Clear() {
	h.first = 2 << 6
	h.second = 0
	h.sequenceNumber = 0
	h.timestamp = 0
	h.ssrc = 0
	h.csrcs.Clear()
	h.extension.Clear()
}

type Payload struct {
	ioUtil.OpWriter
}

func NewPayload(size int) *Payload {
	p := new(Payload)
	p.SetBuf(make([]byte, size))
	return p
}

func PayloadProvider(size int) func() PayloadI {
	return func() PayloadI { return NewPayload(size) }
}

type ChunksPayload struct {
	chunks [][]byte
	size   int
}

func NewChunksPayload() *ChunksPayload { return &ChunksPayload{} }
func ProvideChunksPayload() PayloadI   { return NewChunksPayload() }

func (p *ChunksPayload) Chunks() [][]byte {
	return p.chunks
}

func (p *ChunksPayload) Range(f func(chunk []byte) bool) bool {
	for _, chunk := range p.chunks {
		if !f(chunk) {
			return false
		}
	}
	return true
}

func (p *ChunksPayload) Append(chunk []byte) {
	p.chunks = append(p.chunks, chunk)
	p.size += len(chunk)
}

func (p *ChunksPayload) Size() int {
	return p.size
}

func (p *ChunksPayload) Bytes() []byte {
	w := ioUtil.Writer{Buf: make([]byte, p.size)}
	n, _ := p.WriteTo(&w)
	return w.Buf[:n]
}

func (p *ChunksPayload) WriteTo(w io.Writer) (n int64, err error) {
	for _, chunk := range p.chunks {
		if err = ioUtil.Write(w, chunk, &n); err != nil {
			return
		}
	}
	return
}

func (p *ChunksPayload) Clear() {
	p.chunks = p.chunks[:0]
	p.size = 0
}

type layer struct {
	*Header
	payload PayloadI
	padding Padding
}

func (l *layer) Payload() PayloadI {
	return l.payload
}

func (l *layer) PaddingLength() uint8 {
	return l.padding.Len()
}

func (l *layer) SetPaddingLength(length uint8) {
	l.padding = Padding(length)
	l.setPadding(length != 0)
}

func (l *layer) Size() int {
	var length int
	if l.Header.HasExtension() {
		length += l.Header.extension.Size()
	}
	if l.Header.HasPadding() {
		length += int(l.padding)
	}
	length += BaseHeaderSize + l.Header.csrcs.Size() + l.payload.Size()
	return length
}

func (l *layer) Bytes() []byte {
	w := ioUtil.Writer{Buf: make([]byte, l.Size())}
	n, _ := l.WriteTo(&w)
	return w.Buf[:n]
}

func (l *layer) WriteTo(w io.Writer) (n int64, err error) {
	// write header
	if err = ioUtil.WriteTo(l.Header, w, &n); err != nil {
		return
	}
	// write payload content
	if err = ioUtil.WriteTo(l.payload, w, &n); err != nil {
		return
	}
	// write padding
	if l.Header.HasPadding() && l.padding > 0 {
		if err = ioUtil.WriteTo(l.padding, w, &n); err != nil {
			return
		}
	}
	return
}

func (l *layer) Clear() {
	l.Header.Clear()
	l.padding = 0
	if l.payload != nil {
		l.payload.Clear()
	}
}

func (l *layer) String() string {
	sb := strings.Builder{}
	sb.WriteString("RTP(Length=")
	sb.WriteString(strconv.FormatUint(uint64(l.Size()), 10))
	sb.WriteString(", PT=")
	sb.WriteString(strconv.FormatUint(uint64(l.Header.PayloadType()), 10))
	sb.WriteString(", SSRC=0x")
	sb.WriteString(strconv.FormatUint(uint64(l.Header.ssrc), 16))
	if csrcCount := len(l.Header.csrcs); csrcCount > 0 {
		sb.WriteString(", CSRC=[")
		for i := 0; i < csrcCount; i++ {
			sb.WriteString("0x")
			sb.WriteString(strconv.FormatUint(uint64(l.Header.csrcs[i]), 16))
			if i != csrcCount-1 {
				sb.WriteString(", ")
			}
		}
		sb.WriteString("]")
	}
	sb.WriteString(", Seq=")
	sb.WriteString(strconv.FormatUint(uint64(l.Header.sequenceNumber), 10))
	sb.WriteString(", Time=")
	sb.WriteString(strconv.FormatUint(uint64(l.Header.timestamp), 10))
	sb.WriteString(", Payload=")
	sb.WriteString(strconv.FormatUint(uint64(l.payload.Size()), 10))
	if l.Header.HasPadding() {
		sb.WriteString(", Padding")
	}
	if l.Header.HasExtension() {
		sb.WriteString(", Extent")
	}
	if l.Header.Marker() {
		sb.WriteString(", Mark")
	}
	sb.WriteString(")")
	return sb.String()
}

type Layer struct {
	Header
	payload PayloadI
	padding Padding
}

func NewLayer(payload PayloadI) *Layer {
	return &Layer{Header: MakeHeader(), payload: payload}
}

func LayerProvider(payloadProvider func() PayloadI) func() LayerI {
	return func() LayerI { return NewLayer(payloadProvider()) }
}

func (l *Layer) Init(payload PayloadI) {
	l.first = 2 << 6
	l.payload = payload
}

func (l *Layer) Payload() PayloadI {
	return l.payload
}

func (l *Layer) SetPayload(payload PayloadI) {
	l.payload = payload
}

func (l *Layer) PaddingLength() uint8 {
	return uint8(l.padding)
}

func (l *Layer) SetPaddingLength(length uint8) {
	l.padding = Padding(length)
	l.setPadding(length != 0)
}

func (l *Layer) Size() int {
	return (&layer{
		Header:  &l.Header,
		payload: l.payload,
		padding: l.padding,
	}).Size()
}

func (l *Layer) Bytes() []byte {
	return (&layer{
		Header:  &l.Header,
		payload: l.payload,
		padding: l.padding,
	}).Bytes()
}

func (l *Layer) WriteTo(w io.Writer) (n int64, err error) {
	return (&layer{
		Header:  &l.Header,
		payload: l.payload,
		padding: l.padding,
	}).WriteTo(w)
}

func (l *Layer) Clear() {
	(&layer{
		Header:  &l.Header,
		payload: l.payload,
		padding: l.padding,
	}).Clear()
}

func (l *Layer) String() string {
	return (&layer{
		Header:  &l.Header,
		payload: l.payload,
		padding: l.padding,
	}).String()
}

type InputLayer struct {
	Header
	payload ChunksPayload
	padding Padding
}

func NewInputLayer() *InputLayer { return &InputLayer{Header: MakeHeader()} }
func ProvideInputLayer() LayerI  { return NewInputLayer() }

func (l *InputLayer) Size() int {
	return (&layer{
		Header:  &l.Header,
		payload: &l.payload,
		padding: l.padding,
	}).Size()
}

func (l *InputLayer) Payload() PayloadI {
	return &l.payload
}

func (l *InputLayer) PaddingLength() uint8 {
	return uint8(l.padding)
}

func (l *InputLayer) SetPaddingLength(length uint8) {
	l.padding = Padding(length)
	l.first |= 0b00100000
}

func (l *InputLayer) Unmarshal(data []byte) error {
	parser := InputLayerParser{}
	if err := parser.Init(l, data); err != nil {
		return err
	}
	return parser.Parse(data)
}

func (l *InputLayer) UnmarshalChunks(cs [][]byte) error {
	parser := InputLayerParser{}
	if err := parser.InitChunks(l, cs); err != nil {
		return err
	}
	return parser.ParseChunks(cs)
}

func (l *InputLayer) Bytes() []byte {
	return (&layer{
		Header:  &l.Header,
		payload: &l.payload,
		padding: l.padding,
	}).Bytes()
}

func (l *InputLayer) WriteTo(w io.Writer) (n int64, err error) {
	return (&layer{
		Header:  &l.Header,
		payload: &l.payload,
		padding: l.padding,
	}).WriteTo(w)
}

func (l *InputLayer) Clear() {
	l.Header.Clear()
	l.padding = 0
	l.payload.Clear()
}

func (l *InputLayer) String() string {
	return (&layer{
		Header:  &l.Header,
		payload: &l.payload,
		padding: l.padding,
	}).String()
}
