package rtp

import (
	"gitee.com/sy_183/go-common/pool"
	"net"
	"time"
)

// PacketI 接口在 LayerI 接口的基础上定义了关联引用计数的相关行为
type PacketI interface {
	LayerI
	pool.RelationsReference
}

func UsePacket(packet PacketI) PacketI {
	packet.AddRef()
	return packet
}

type Packet struct {
	Layer
	relations pool.Relations
	pool      pool.Pool[*Packet]
}

func NewPacket(pool pool.Pool[*Packet], payload PayloadI) *Packet {
	p := &Packet{pool: pool}
	p.Init(payload)
	return p
}

func PacketProvider(payloadProvider func() PayloadI) func(pool pool.Pool[*Packet]) *Packet {
	return func(pool pool.Pool[*Packet]) *Packet {
		return NewPacket(pool, payloadProvider())
	}
}

func (p *Packet) AddRelation(relation pool.Reference) {
	p.relations.AddRelation(relation)
}

func (p *Packet) Clear() {
	p.relations.Clear()
	p.Layer.Clear()
}

func (p *Packet) Release() bool {
	if p.relations.Release() {
		p.Clear()
		if p.pool != nil {
			p.pool.Put(p)
		}
		return true
	}
	return false
}

func (p *Packet) AddRef() {
	p.relations.AddRef()
}

func (p *Packet) Use() *Packet {
	p.AddRef()
	return p
}

// InputPacket 通常用于承载外部接受到的RTP包，可以标记对端地址、接收时间戳等信息，并且使用
// InputLayer 作为RTP包的载体
type InputPacket struct {
	*InputLayer
	addr      net.Addr
	time      time.Time
	relations pool.Relations
	pool      pool.Pool[*InputPacket]
}

func NewInputPacket(layer *InputLayer, pool pool.Pool[*InputPacket]) *InputPacket {
	return &InputPacket{InputLayer: layer, pool: pool}
}

func ProvideInputPacket(p pool.Pool[*InputPacket]) *InputPacket {
	return NewInputPacket(NewInputLayer(), p)
}

func (p *InputPacket) Addr() net.Addr {
	return p.addr
}

func (p *InputPacket) SetAddr(addr net.Addr) {
	p.addr = addr
}

func (p *InputPacket) Time() time.Time {
	return p.time
}

func (p *InputPacket) SetTime(t time.Time) {
	p.time = t
}

func (p *InputPacket) AddRelation(relation pool.Reference) {
	p.relations.AddRelation(relation)
}

func (p *InputPacket) Clear() {
	p.relations.Clear()
	// 不需要清除InputLayer，
	p.addr = nil
	p.time = time.Time{}
}

func (p *InputPacket) Release() bool {
	if p.relations.Release() {
		p.Clear()
		if p.pool != nil {
			p.pool.Put(p)
		}
		return true
	}
	return false
}

func (p *InputPacket) AddRef() {
	p.relations.AddRef()
}

func (p *InputPacket) Use() *InputPacket {
	p.AddRef()
	return p
}
