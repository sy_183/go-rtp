package rtp

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidVersion         = errors.New("invalid RTP version")
	ErrInvalidRTSPPrefix      = errors.New("invalid RTP RTSP prefix")
	ErrInvalidCSRCLength      = errors.New("invalid RTP CSRC length")
	ErrInvalidExtensionLength = errors.New("invalid RTP extension length")
	ErrInvalidPayloadLength   = errors.New("invalid RTP payload length")
	ErrInvalidPaddingLength   = errors.New("invalid RTP padding length")
	ErrInvalidPacketSize      = errors.New("invalid RTP packet size")
)

type RtpPacketError struct {
	Err error
}

func (e RtpPacketError) Error() string {
	return fmt.Sprintf("invalid rtp packet: %s", e.Err.Error())
}

func (e RtpPacketError) Unwrap() error {
	return e.Err
}

var (
	ErrBufAllocFailed       = errors.New("buf alloc failed")
	ErrPacketDropped        = errors.New("RTP packet dropped")
	ErrTooManyInvalidPacket = errors.New("too many invalid RTP packet")
	ErrStreamExist          = errors.New("RTP stream already exists")
)
