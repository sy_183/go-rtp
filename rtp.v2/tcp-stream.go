package rtp

import (
	"net"
	"sync/atomic"
	"time"
)

type TCPStream struct {
	id      string
	channel atomic.Pointer[TCPPassiveChannel]
	server  atomic.Pointer[TCPServer]
	closed  atomic.Bool

	localAddr  atomic.Pointer[net.TCPAddr]
	remoteAddr atomic.Pointer[net.TCPAddr]
	ssrc       atomic.Int64
	timeout    time.Duration

	lastTime time.Time

	isInit bool
	seq    uint16

	streamHandler
	ctx any
}

func (s *TCPStream) setTimeout(timeout time.Duration) {
	s.timeout = timeout
}

func (s *TCPStream) setContext(ctx any) {
	s.ctx = ctx
}

func (s *TCPStream) deadline() time.Time {
	if s.timeout > 0 {
		return s.lastTime.Add(s.timeout)
	}
	return zeroTime
}

func (s *TCPStream) doHandlePacket(packet *InputPacket) error {
	if s.keepChooser == nil {
		s.keepChooser = NewDefaultKeepChooser()
	}
	s.keepChooser.OnSuccess()

	tAddr, is := packet.Addr().(*net.TCPAddr)
	if !is {
		packet.Release()
		return ErrPacketDropped
	}

	var rAddr *net.TCPAddr
	if !s.remoteAddr.CompareAndSwap(nil, tAddr) {
		rAddr = s.remoteAddr.Load()
		if !rAddr.IP.Equal(tAddr.IP) ||
			rAddr.Port != tAddr.Port ||
			rAddr.Zone != tAddr.Zone {
			// packet addr not matched, dropped
			packet.Release()
			return ErrPacketDropped
		}
	}
	rSsrc := packet.SSRC()
	if !s.ssrc.CompareAndSwap(-1, int64(rSsrc)) {
		ssrc := s.ssrc.Load()
		if ssrc != int64(rSsrc) {
			packet.Release()
			return ErrPacketDropped
		}
	}

	// count rtp packet loss
	if !s.isInit {
		s.isInit = true
	} else {
		if diff := packet.SequenceNumber() - s.seq; diff != 1 {
			s.doOnLossPacket(int(diff) - 1)
		}
	}
	s.seq = packet.SequenceNumber()
	if s.packetHandler != nil {
		return s.packetHandler(s, packet)
	}
	packet.Release()
	return nil
}

func (s *TCPStream) doOnParseError(err error) error {
	if s.onParseError != nil {
		if err := s.onParseError(s, err); err != nil {
			return err
		}
	}
	if s.keepChooser == nil {
		s.keepChooser = NewDefaultKeepChooser()
	}
	if !s.keepChooser.OnError(err) {
		return ErrTooManyInvalidPacket
	}
	return nil
}

func (s *TCPStream) doOnLossPacket(loss int) {
	if s.onLossPacket != nil {
		s.onLossPacket(s, loss)
	}
}

func (s *TCPStream) doOnTimeout() {
	if s.onTimeout != nil {
		s.onTimeout(s)
	}
}

func (s *TCPStream) doOnClose() {
	if s.onClose != nil {
		s.onClose(s)
	}
}

func (s *TCPStream) SSRC() int64 {
	return s.ssrc.Load()
}

func (s *TCPStream) SetSSRC(ssrc int64) Stream {
	s.ssrc.Store(ssrc)
	return s
}

func (s *TCPStream) LocalAddr() net.Addr {
	return s.localAddr.Load()
}

func (s *TCPStream) RemoteAddr() net.Addr {
	return s.remoteAddr.Load()
}

func (s *TCPStream) SetRemoteAddr(addr net.Addr) Stream {
	s.remoteAddr.Store(addr.(*net.TCPAddr))
	return s
}

func (s *TCPStream) Context() any {
	return s.ctx
}

func (s *TCPStream) Close() error {
	if s.closed.CompareAndSwap(false, true) {
		if channel := s.channel.Load(); channel != nil {
			return channel.Close()
		} else if server := s.server.Load(); server != nil {
			if s.server.CompareAndSwap(server, nil) {
				if stream, exist := server.streams.LoadAndDelete(s.id); exist {
					stream.doOnClose()
				}
			}
		}
	}
	return nil
}
