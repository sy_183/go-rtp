package rtp

import (
	"errors"
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"gitee.com/sy_183/go-common/lifecycle.v2/future"
	"gitee.com/sy_183/go-common/option"
	"gitee.com/sy_183/go-common/pool"
	"net"
	"sync/atomic"
	"time"
)

type TCPActiveChannel struct {
	lifecycle.Lifecycle
	interrupted atomic.Bool

	conn       atomic.Pointer[net.TCPConn]
	localAddr  atomic.Pointer[net.TCPAddr]
	remoteAddr *net.TCPAddr
	stream     atomic.Pointer[TCPStream]

	startTime time.Time
	timeout   time.Duration

	readBufferPool pool.BufferPool
	packetPool     pool.Pool[*InputPacket]

	keepChooser KeepChooser
	parser      StreamParser
	packet      *InputPacket

	channelActionHandler ChannelActionHandler
	serverActionHandler  ServerActionHandler
	ctx                  any
}

func NewTCPActiveChannel(localAddr, remoteAddr *net.TCPAddr, options ...option.AnyOption) *TCPActiveChannel {
	c := &TCPActiveChannel{
		remoteAddr: remoteAddr,
	}
	c.localAddr.Store(localAddr)

	for _, opt := range options {
		opt.Apply(c)
	}
	if c.readBufferPool == nil {
		c.readBufferPool = pool.NewDefaultBufferPool(DefaultBufferPoolSize, DefaultBufferReverse, pool.ProvideSyncPool[*pool.Buffer])
	}
	if c.packetPool == nil {
		c.setPacketPoolProvider(pool.ProvideSyncPool[*InputPacket])
	}
	if c.keepChooser == nil {
		c.keepChooser = NewDefaultKeepChooser()
	}
	c.Lifecycle = lifecycle.NewWithFunc(c.start, c.run, c.close)
	return c
}

func (c *TCPActiveChannel) setTimeout(timeout time.Duration) {
	c.timeout = timeout
}

func (c *TCPActiveChannel) setPacketPoolProvider(provider pool.PoolProvider[*InputPacket]) {
	c.packetPool = provider(ProvideInputPacket)
}

func (c *TCPActiveChannel) setReadBufferPool(bufferPool pool.BufferPool) {
	c.readBufferPool = bufferPool
}

func (c *TCPActiveChannel) setReadBufferPoolProvider(provider func() pool.BufferPool) {
	c.readBufferPool = provider()
}

func (c *TCPActiveChannel) setKeepChooser(chooser KeepChooser) {
	c.keepChooser = chooser
}

func (c *TCPActiveChannel) setChannelActionHandler(handler ChannelActionHandler) {
	c.channelActionHandler = handler
}

func (c *TCPActiveChannel) setContext(ctx any) {
	c.ctx = ctx
}

func (c *TCPActiveChannel) Addr() net.Addr {
	return c.localAddr.Load()
}

func (c *TCPActiveChannel) LocalAddr() net.Addr {
	return c.localAddr.Load()
}

func (c *TCPActiveChannel) RemoteAddr() net.Addr {
	return c.remoteAddr
}

func (c *TCPActiveChannel) Context() any {
	return c.ctx
}

func (c *TCPActiveChannel) Conn() *net.TCPConn {
	return c.conn.Load()
}

func (c *TCPActiveChannel) channelAction(action Action, err error, args ...any) {
	if c.channelActionHandler != nil {
		c.channelActionHandler(c, action, err, args...)
	}
}

func (c *TCPActiveChannel) serverAction(action Action, err error, args ...any) {
	if c.serverActionHandler != nil {
		c.serverActionHandler(c, action, err, args...)
	}
}

func (c *TCPActiveChannel) read(conn *net.TCPConn) (ok bool, err error) {
	readBufferPool := c.readBufferPool
	buf := readBufferPool.Get()
	if buf == nil {
		// TCP读取缓冲区申请失败，可能是因为流量过大导致，所以可以利用缓冲区做限流
		c.channelAction(ActionAllocBuf, ErrBufAllocFailed)
		c.doClose(conn)
		return false, ErrBufAllocFailed
	}

	preStream := c.stream.Load()
	var deadline time.Time
	var channelWillTimeout bool
	if preStream != nil {
		deadline = preStream.deadline()
	} else {
		if now := time.Now(); c.timeout > 0 {
			deadline = c.startTime.Add(c.timeout)
			if du := deadline.Sub(now); du > time.Second {
				deadline = now.Add(time.Second)
			} else if du < 0 {
				c.doClose(conn)
				return false, nil
			} else {
				channelWillTimeout = true
			}
		} else {
			deadline = now.Add(time.Second)
		}
	}
	if err := conn.SetReadDeadline(deadline); err != nil {
		c.channelAction(ActionSetReadDeadline, err, deadline)
	}

	n, err := conn.Read(buf)
	now := time.Now()
	stream := c.stream.Load()
	if err != nil {
		if netErr, is := err.(net.Error); is && netErr.Timeout() {
			// preStream 为读取到数据之前的RTP流。如果 preStream 为 nil，则此时超时为默认
			// UDP通道的超时，超时时间固定为1s，一定小于等于RTP流的设置的超时时间，所以这种情
			// 况下，即使当前RTP流不为 nil，RTP流也一定不会超时。如果 preStream 不为 nil，
			// 那么此时超时一定是RTP流的超时
			if preStream != nil {
				stream.doOnTimeout()
				c.doClose(conn)
				return false, nil
			}
			if channelWillTimeout && stream == nil {
				// 通道已经到了超时的时间，并且在此期间没有流添加到通道中，因此通道超时
				c.channelAction(ActionTimeout, nil)
				c.doClose(conn)
				return false, nil
			}
			return !c.interrupted.Load(), nil
		}
		if errors.Is(err, net.ErrClosed) {
			return false, nil
		}
		c.channelAction(ActionRead, err)
		c.doClose(conn)
		return false, err
	}
	data := readBufferPool.Alloc(uint(n))
	defer func() {
		data.Release()
		if !ok {
			c.doClose(conn)
		}
	}()

	for p := data.Data; len(p) > 0; {
		var parsed bool
		if c.packet == nil {
			// 从RTP包的池中获取，并且添加至解析器
			c.packet = c.packetPool.Get().Use()
			c.parser.Layer = c.packet.InputLayer
		}
		if parsed, p, err = c.parser.Parse(p); parsed {
			// 解析RTP包成功
			c.keepChooser.OnSuccess()
			c.packet.AddRelation(data.Use())
			c.packet.SetAddr(c.remoteAddr)
			c.packet.SetTime(now)
			if stream != nil {
				// 将RTP包交给流处理，流处理器必须在使用完RTP包后将其释放
				if err = stream.doHandlePacket(c.packet); err != nil {
					if err != ErrPacketDropped {
						return false, err
					}
				} else {
					// 未丢包的情况下，才设置收包时间用于下一次超时的判断
					stream.lastTime = now
				}
				c.packet = nil
			} else {
				// 没有处理RTP包的流，释放RTP包中的数据，然后继续使用此RTP包作为接下来解析器解析
				// 的载体
				c.packet.Clear()
			}
		} else if err != nil {
			// 解析RTP包出错，释放RTP包中的数据，如果可以继续解析则使用此RTP包作为接下来解析器解析
			// 的载体
			c.packet.Clear()
			if c.keepChooser.OnError(err) {
				return false, ErrTooManyInvalidPacket
			}
			if stream != nil {
				// 使用RTP流处理解析错误，并决定是否继续解析
				if err = stream.doOnParseError(err); err != nil {
					return false, err
				}
			}
		} else {
			// 解析RTP包未完成，需要将此块数据的引用添加到此RTP包中
			c.packet.AddRelation(data.Use())
		}
	}
	return true, nil
}

func (c *TCPActiveChannel) start(lifecycle.Lifecycle) (err error) {
	conn := c.conn.Load()
	if conn == nil {
		conn, err = net.DialTCP("tcp", c.localAddr.Load(), c.remoteAddr)
		if err != nil {
			c.serverAction(ActionDial, err)
			return err
		}
		c.conn.Store(conn)
		if tAddr, is := conn.LocalAddr().(*net.TCPAddr); is {
			c.localAddr.Store(tAddr)
			if stream := c.stream.Load(); stream != nil {
				stream.localAddr.Store(tAddr)
			}
		}
		c.serverAction(ActionDial, nil)
		if c.interrupted.Load() {
			c.doClose(conn)
			return lifecycle.ErrInterrupted
		}
	}
	now := time.Now()
	if stream := c.stream.Load(); stream != nil {
		stream.lastTime = now
	}
	c.startTime = now
	return nil
}

func (c *TCPActiveChannel) run(lifecycle.Lifecycle) error {
	conn := c.conn.Load()
	for {
		if ok, err := c.read(conn); !ok {
			return err
		}
	}
}

func (c *TCPActiveChannel) doClose(conn *net.TCPConn) error {
	err := conn.Close()
	if err != nil {
		if errors.Is(err, net.ErrClosed) {
			return nil
		}
	}
	c.channelAction(ActionClose, err)
	c.serverAction(ActionClose, err)
	return err
}

func (c *TCPActiveChannel) close(lifecycle.Lifecycle) error {
	if c.interrupted.CompareAndSwap(false, true) {
		if listener := c.conn.Load(); listener != nil {
			return c.doClose(listener)
		}
	}
	return nil
}

func (c *TCPActiveChannel) Stream(remoteAddr net.Addr, ssrc int64, options ...StreamOption) (Stream, error) {
	if stream := c.stream.Load(); stream != nil {
		return stream, ErrStreamExist
	}
	stream := &TCPStream{}

	stream.localAddr.Store(c.localAddr.Load())
	stream.SetRemoteAddr(remoteAddr)
	stream.SetSSRC(ssrc)
	stream.lastTime = time.Now()

	for _, opt := range options {
		opt.Apply(stream)
	}

	if !c.stream.CompareAndSwap(nil, stream) {
		return c.stream.Load(), ErrStreamExist
	}

	c.Future(lifecycle.FutureTypeClosed, future.CallbackFuture[error](func(err error) {
		stream.doOnClose()
	}))

	return stream, nil
}
