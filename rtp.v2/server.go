package rtp

import (
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"net"
)

type Server interface {
	lifecycle.Lifecycle

	Addr() net.Addr

	Stream(remoteAddr net.Addr, ssrc int64, options ...StreamOption) (Stream, error)
}
