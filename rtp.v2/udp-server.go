package rtp

import (
	"errors"
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"gitee.com/sy_183/go-common/lifecycle.v2/future"
	"gitee.com/sy_183/go-common/option"
	"gitee.com/sy_183/go-common/pool"
	"net"
	"sync/atomic"
	"time"
)

type UDPServer struct {
	lifecycle.Lifecycle
	interrupted atomic.Bool

	addr     *net.UDPAddr
	listener atomic.Pointer[net.UDPConn]
	stream   atomic.Pointer[UDPStream]

	startTime time.Time
	timeout   time.Duration

	readBufferPool pool.BufferPool
	packetPool     pool.Pool[*InputPacket]

	channelActionHandler ChannelActionHandler
	serverActionHandler  ServerActionHandler
	ctx                  any
}

func NewUDPServer(addr *net.UDPAddr, options ...option.AnyOption) *UDPServer {
	s := &UDPServer{
		addr: addr,
	}

	for _, opt := range options {
		opt.Apply(s)
	}
	if s.addr == nil {
		s.addr = &net.UDPAddr{IP: net.IP{0, 0, 0, 0}, Port: 5004}
	}
	if s.readBufferPool == nil {
		s.readBufferPool = pool.NewDefaultBufferPool(DefaultBufferPoolSize, DefaultBufferReverse, pool.ProvideSyncPool[*pool.Buffer])
	}
	if s.packetPool == nil {
		s.setPacketPoolProvider(pool.ProvideSyncPool[*InputPacket])
	}
	s.Lifecycle = lifecycle.NewWithFunc(s.start, s.run, s.close)
	return s
}

func (s *UDPServer) setTimeout(timeout time.Duration) {
	s.timeout = timeout
}

func (s *UDPServer) setServerActionHandler(handler ServerActionHandler) {
	s.serverActionHandler = handler
}

func (s *UDPServer) setChannelOptions(options ...option.AnyOption) {
	for _, opt := range options {
		opt.Apply(s)
	}
}

func (s *UDPServer) appendChannelOptions(options ...option.AnyOption) {
	for _, opt := range options {
		opt.Apply(s)
	}
}

func (s *UDPServer) setPacketPoolProvider(provider pool.PoolProvider[*InputPacket]) {
	s.packetPool = provider(ProvideInputPacket)
}

func (s *UDPServer) setReadBufferPool(bufferPool pool.BufferPool) {
	s.readBufferPool = bufferPool
}

func (s *UDPServer) setReadBufferPoolProvider(provider func() pool.BufferPool) {
	s.readBufferPool = provider()
}

func (s *UDPServer) setChannelActionHandler(handler ChannelActionHandler) {
	s.channelActionHandler = handler
}

func (s *UDPServer) setContext(ctx any) {
	s.ctx = ctx
}

func (s *UDPServer) Addr() net.Addr {
	return s.addr
}

func (s *UDPServer) Listener() *net.UDPConn {
	return s.listener.Load()
}

func (s *UDPServer) LocalAddr() net.Addr {
	return s.addr
}

func (s *UDPServer) RemoteAddr() net.Addr {
	return nil
}

func (s *UDPServer) Context() any {
	return s.ctx
}

func (s *UDPServer) channelAction(action Action, err error, args ...any) {
	if s.channelActionHandler != nil {
		s.channelActionHandler(s, action, err, args...)
	}
}

func (s *UDPServer) serverAction(action Action, err error, args ...any) {
	if s.serverActionHandler != nil {
		s.serverActionHandler(s, action, err, args...)
	}
}

func (s *UDPServer) read(conn *net.UDPConn) (ok bool, err error) {
	readBufferPool := s.readBufferPool
	buf := readBufferPool.Get()
	if buf == nil {
		// UDP读取缓冲区申请失败，可能是因为流量过大导致，所以可以利用缓冲区做限流
		s.channelAction(ActionAllocBuf, ErrBufAllocFailed)
		s.doClose(conn)
		return false, ErrBufAllocFailed
	}

	preStream := s.stream.Load()
	var deadline time.Time
	var channelWillTimeout bool
	if preStream != nil {
		deadline = preStream.deadline()
	} else {
		if now := time.Now(); s.timeout > 0 {
			deadline = s.startTime.Add(s.timeout)
			if du := deadline.Sub(now); du > time.Second {
				deadline = now.Add(time.Second)
			} else if du < 0 {
				s.doClose(conn)
				return false, nil
			} else {
				channelWillTimeout = true
			}
		} else {
			deadline = now.Add(time.Second)
		}
	}
	if err := conn.SetReadDeadline(deadline); err != nil {
		s.channelAction(ActionSetReadDeadline, err, deadline)
	}

	n, addr, err := conn.ReadFromUDP(buf)
	now := time.Now()
	stream := s.stream.Load()
	if err != nil {
		if netErr, is := err.(net.Error); is && netErr.Timeout() {
			// preStream 为读取到数据之前的RTP流。如果 preStream 为 nil，则此时超时为默认
			// UDP通道的超时，超时时间固定为1s，一定小于等于RTP流的设置的超时时间，所以这种情
			// 况下，即使当前RTP流不为 nil，RTP流也一定不会超时。如果 preStream 不为 nil，
			// 那么此时超时一定是RTP流的超时
			if preStream != nil {
				stream.doOnTimeout()
				s.doClose(conn)
				return false, nil
			}
			if channelWillTimeout && stream == nil {
				// 通道已经到了超时的时间，并且在此期间没有流添加到通道中，因此通道超时
				s.channelAction(ActionTimeout, nil, s.timeout)
				s.doClose(conn)
				return false, nil
			}
			return !s.interrupted.Load(), nil
		}
		if errors.Is(err, net.ErrClosed) {
			return false, nil
		}
		s.channelAction(ActionRead, err)
		s.doClose(conn)
		return false, err
	}
	data := readBufferPool.Alloc(uint(n))

	if stream != nil {
		packet := s.packetPool.Get().Use()
		if err = packet.Unmarshal(data.Data); err != nil {
			data.Release()
			packet.Release()
			if err = stream.doOnParseError(err); err != nil {
				return false, err
			}
			return true, nil
		}
		packet.SetAddr(addr)
		packet.SetTime(now)
		packet.AddRelation(data)
		if err = stream.doHandlePacket(packet); err != nil {
			if err != ErrPacketDropped {
				return false, err
			}
		} else {
			// 未丢包的情况下，才设置收包时间用于下一次超时的判断
			stream.lastTime = now
		}
		return true, nil
	}

	data.Release()
	return true, nil
}

func (s *UDPServer) start(lifecycle.Lifecycle) (err error) {
	listener := s.listener.Load()
	if listener == nil {
		listener, err = net.ListenUDP("udp", s.addr)
		if err != nil {
			s.serverAction(ActionListen, err)
			return err
		}
		s.listener.Store(listener)
		s.serverAction(ActionListen, nil)
		if s.interrupted.Load() {
			s.doClose(listener)
			return lifecycle.ErrInterrupted
		}
	}
	now := time.Now()
	if stream := s.stream.Load(); stream != nil {
		stream.lastTime = now
	}
	s.startTime = now
	return nil
}

func (s *UDPServer) run(lifecycle.Lifecycle) error {
	conn := s.listener.Load()
	for {
		if ok, err := s.read(conn); !ok {
			return err
		}
	}
}

func (s *UDPServer) doClose(listener *net.UDPConn) error {
	err := listener.Close()
	if err != nil {
		if errors.Is(err, net.ErrClosed) {
			return nil
		}
	}
	s.channelAction(ActionClose, err)
	s.serverAction(ActionClose, err)
	return err
}

func (s *UDPServer) close(lifecycle.Lifecycle) error {
	if s.interrupted.CompareAndSwap(false, true) {
		if listener := s.listener.Load(); listener != nil {
			return s.doClose(listener)
		}
	}
	return nil
}

func (s *UDPServer) Stream(remoteAddr net.Addr, ssrc int64, options ...StreamOption) (Stream, error) {
	if stream := s.stream.Load(); stream != nil {
		return stream, ErrStreamExist
	}
	stream := &UDPStream{
		server:    s,
		localAddr: s.addr,
	}

	stream.SetRemoteAddr(remoteAddr)
	stream.SetSSRC(ssrc)
	stream.lastTime = time.Now()

	for _, opt := range options {
		opt.Apply(stream)
	}

	if !s.stream.CompareAndSwap(nil, stream) {
		return s.stream.Load(), ErrStreamExist
	}

	s.Future(lifecycle.FutureTypeClosed, future.CallbackFuture[error](func(err error) {
		stream.doOnClose()
	}))

	return stream, nil
}
