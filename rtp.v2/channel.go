package rtp

import (
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"net"
)

type Channel interface {
	lifecycle.Lifecycle

	LocalAddr() net.Addr

	RemoteAddr() net.Addr

	Context() any
}
