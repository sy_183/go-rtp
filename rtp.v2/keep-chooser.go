package rtp

import (
	"gitee.com/sy_183/go-common/container"
	"time"
)

type KeepChooser interface {
	OnSuccess()

	OnError(err error) (keep bool)

	Reset()
}

type keepChooser struct {
	secErrorCounter  *container.Queue[time.Time]
	maxSerializedErr int
	serializedErrors int
	onNotKeep        func()
}

func NewDefaultKeepChooser() KeepChooser {
	return NewKeepChooser(-1, -1, nil)
}

func NewKeepChooser(secMaxErr int, maxSerializedErr int, onNotKeep func()) KeepChooser {
	if secMaxErr < 0 {
		secMaxErr = 5
	} else if secMaxErr > 100 {
		secMaxErr = 100
	}
	if maxSerializedErr < 0 {
		maxSerializedErr = 5
	}
	return &keepChooser{
		secErrorCounter:  container.NewQueue[time.Time](secMaxErr),
		maxSerializedErr: maxSerializedErr,
		onNotKeep:        onNotKeep,
	}
}

func (c *keepChooser) OnSuccess() {
	if c.serializedErrors > 0 {
		c.serializedErrors = 0
	}
}

func (c *keepChooser) OnError(err error) (keep bool) {
	defer func() {
		if !keep {
			if c.onNotKeep != nil {
				c.onNotKeep()
			}
		}
	}()
	if c.serializedErrors++; c.serializedErrors > c.maxSerializedErr {
		return false
	}
	now := time.Now()
	for {
		head, exist := c.secErrorCounter.Head()
		if exist && now.Sub(head) > time.Second {
			c.secErrorCounter.Shift()
		} else {
			break
		}
	}
	if !c.secErrorCounter.Push(now) {
		return false
	}
	return true
}

func (c *keepChooser) Reset() {
	c.secErrorCounter.Clear()
	c.serializedErrors = 0
}

type KeepChooserHandler struct {
	PacketHandler PacketHandler
	OnParseError  ParseErrorHandler
	KeepChooser   KeepChooser
}

func (h KeepChooserHandler) HandlePacket(stream Stream, packet *InputPacket) error {
	h.KeepChooser.OnSuccess()
	if h.PacketHandler != nil {
		return h.PacketHandler(stream, packet)
	}
	return nil
}

func (h KeepChooserHandler) HandleParseError(stream Stream, err error) error {
	if h.OnParseError != nil {
		if err := h.OnParseError(stream, err); err != nil {
			return err
		}
	}
	if !h.KeepChooser.OnError(err) {
		return ErrTooManyInvalidPacket
	}
	return nil
}
