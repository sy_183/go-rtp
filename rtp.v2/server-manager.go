package rtp

import (
	"errors"
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"gitee.com/sy_183/go-common/lifecycle.v2/future"
	"gitee.com/sy_183/go-common/option"
	"net"
	"sort"
	"sync/atomic"
)

var (
	ErrInvalidServerAllocMode    = errors.New("invalid server alloc mode")
	ErrServerManagerNotAvailable = errors.New("server manager not available")
	ErrServerAlloc               = errors.New("server alloc failed")
	ErrServerRepeatFree          = errors.New("server repeat free")
)

type ServerContext struct {
	manager *ServerManager
	server  Server
	used    atomic.Int32
}

func (c *ServerContext) Manager() *ServerManager {
	return c.manager
}

func (c *ServerContext) Server() Server {
	return c.server
}

func (c *ServerContext) Used() int {
	return int(c.used.Load())
}

func (c *ServerContext) Free() {
	c.manager.free(c)
}

type ServerProvider func(m *ServerManager, port int, options ...option.AnyOption) Server

func UDPServerProvider(opts ...option.AnyOption) ServerProvider {
	return func(m *ServerManager, port int, options ...option.AnyOption) Server {
		ipAddr := m.Addr()
		return NewUDPServer(&net.UDPAddr{
			IP:   ipAddr.IP,
			Port: port,
			Zone: ipAddr.Zone,
		}, append(options, opts...)...)
	}
}

func TCPServerProvider(opts ...option.AnyOption) ServerProvider {
	return func(m *ServerManager, port int, options ...option.AnyOption) Server {
		ipAddr := m.Addr()
		return NewTCPServer(&net.TCPAddr{
			IP:   ipAddr.IP,
			Port: port,
			Zone: ipAddr.Zone,
		}, append(options, opts...)...)
	}
}

func TCPActiveChannelProvider(remoteAddr *net.TCPAddr, opts ...option.AnyOption) ServerProvider {
	return func(m *ServerManager, port int, options ...option.AnyOption) Server {
		ipAddr := m.Addr()
		return NewTCPActiveChannel(&net.TCPAddr{
			IP:   ipAddr.IP,
			Port: port,
			Zone: ipAddr.Zone,
		}, remoteAddr, append(options, opts...)...)
	}
}

type ServerAllocMode int

const (
	// ServerAllocNext 申请时首先尝试之前申请的下一个端口
	ServerAllocNext = ServerAllocMode(iota)
	// ServerAllocCur 申请时首先尝试之前申请的端口，此端口申请失败尝试下一个端口，以此类推
	ServerAllocCur
)

type serverAllocInfo struct {
	rtpPort  int
	rtcpPort int
}

type ServerManager struct {
	lifecycle.Lifecycle
	interrupted atomic.Bool

	addr          *net.IPAddr
	provider      ServerProvider
	serverOptions []option.AnyOption
	allocMode     ServerAllocMode

	portSet    map[int]struct{}
	allocInfos []serverAllocInfo
	contexts   []atomic.Pointer[ServerContext]
	allocIndex atomic.Int64

	maxUsed       int
	allocMaxRetry int
}

func NewServerManager(addr *net.IPAddr, provider ServerProvider, options ...ServerManagerOption) *ServerManager {
	s := &ServerManager{
		addr:     addr,
		provider: provider,

		portSet: make(map[int]struct{}),
		maxUsed: 1,
	}

	for _, opt := range options {
		opt.Apply(s)
	}

	sort.Slice(s.allocInfos, func(i, j int) bool {
		return s.allocInfos[i].rtpPort < s.allocInfos[j].rtcpPort
	})

	if s.maxUsed < 1 {
		s.maxUsed = 1
	}
	if s.allocMaxRetry == 0 {
		s.allocMaxRetry = len(s.allocInfos)
	}
	s.contexts = make([]atomic.Pointer[ServerContext], len(s.allocInfos))
	s.Lifecycle = lifecycle.NewInterruptedWithFunc(nil, s.run)
	return s
}

func (m *ServerManager) Addr() *net.IPAddr {
	return m.addr
}

func (m *ServerManager) run(_ lifecycle.Lifecycle, interrupter chan struct{}) error {
	defer func() {
		m.interrupted.Load()
		for i := range m.contexts {
			ctx := m.contexts[i].Load()
			if ctx != nil {
				ctx.server.Close()
			}
		}
	}()
	<-interrupter
	return nil
}

func (m *ServerManager) curIndex() int {
	return int(m.allocIndex.Load() % int64(len(m.contexts)))
}

func (m *ServerManager) nextIndex() int {
	return int((m.allocIndex.Add(1) - 1) % int64(len(m.contexts)))
}

func (m *ServerManager) Alloc() (*ServerContext, error) {
	for i := 0; i < m.allocMaxRetry; i++ {
		if m.interrupted.Load() {
			return nil, ErrServerManagerNotAvailable
		}

		var index int
		switch m.allocMode {
		case ServerAllocNext:
			index = m.nextIndex()
		case ServerAllocCur:
			if i == 0 {
				index = m.curIndex()
			} else {
				index = m.nextIndex()
			}
		default:
			return nil, ErrInvalidServerAllocMode
		}
		allocInfo := m.allocInfos[index]

		var ctx *ServerContext
	reload:
		if ctx = m.contexts[index].Load(); ctx == nil {
			// 当前端口下没有服务，需要创建服务
			server := m.provider(m, allocInfo.rtpPort, append([]option.AnyOption(nil), m.serverOptions...)...)
			if server == nil {
				continue
			}
			ctx = &ServerContext{
				manager: m,
				server:  server,
			}
			// 服务在关闭后从管理器中移除
			ctx.server.Future(lifecycle.FutureTypeClosed, future.CallbackFuture[error](func(err error) {
				m.contexts[index].Store(nil)
			}))
			ctx.used.Add(1)
			if !m.contexts[index].CompareAndSwap(nil, ctx) {
				// 服务被其它线程添加，当前创建的服务将被丢弃，并尝试重新获取被添加的服务
				goto reload
			}
			// 此时服务成功被添加
		} else {
			if used := ctx.used.Load(); used == 0 || int(used) >= m.maxUsed {
				// used为0说明服务已经被释放，但是还未从管理器中移除，不可以使用此服务。如果大
				// 于等于最大使用数量，也不可以使用
				continue
			}
			if used := ctx.used.Add(1); int(used) > m.maxUsed {
				// 申请后大于最大使用数量，不可以使用并且要释放此次申请
				m.free(ctx)
				continue
			}
		}
		// 申请服务成功，需要确保当前服务启动成功并处于运行状态，否则不可以使用此服务并且要释放
		// 此次申请
		if err := ctx.server.Start(); err != nil || ctx.server.State() != lifecycle.StateRunning {
			m.free(ctx)
			continue
		}
		if m.interrupted.Load() {
			ctx.server.Close()
			m.free(ctx)
			return nil, ErrServerManagerNotAvailable
		}
		return ctx, nil
	}
	return nil, ErrServerAlloc
}

func (m *ServerManager) free(ctx *ServerContext) {
	if used := ctx.used.Add(-1); used < 0 {
		panic(ErrServerRepeatFree)
	} else if used == 0 {
		ctx.server.Close()
	}
}

func (m *ServerManager) Free(ctx *ServerContext) {
	if ctx.manager != m {
		return
	}
	m.free(ctx)
}
