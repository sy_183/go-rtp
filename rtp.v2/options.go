package rtp

import (
	"gitee.com/sy_183/go-common/option"
	"gitee.com/sy_183/go-common/pool"
	"time"
)

type timeoutSetter interface {
	setTimeout(timeout time.Duration)
}

type keepChooserSetter interface {
	setKeepChooser(chooser KeepChooser)
}

type contextSetter interface {
	setContext(ctx any)
}

func WithServerActionHandler(handler ServerActionHandler) option.AnyOption {
	type serverActionHandlerSetter interface {
		setServerActionHandler(handler ServerActionHandler)
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(serverActionHandlerSetter); is {
			setter.setServerActionHandler(handler)
		}
	})
}

func WithChannelOptions(options ...option.AnyOption) option.AnyOption {
	type channelOptionsSetter interface {
		setChannelOptions(options ...option.AnyOption)
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(channelOptionsSetter); is {
			setter.setChannelOptions(options...)
		}
	})
}

func AppendChannelOptions(options ...option.AnyOption) option.AnyOption {
	type channelOptionsAppender interface {
		appendChannelOptions(options ...option.AnyOption)
	}
	return option.AnyCustom(func(target any) {
		if appender, is := target.(channelOptionsAppender); is {
			appender.appendChannelOptions(options...)
		}
	})
}

func WithChannelTimeout(timeout time.Duration) option.AnyOption {
	if timeout != 0 && timeout < time.Second {
		timeout = time.Second
	}

	return option.AnyCustom(func(target any) {
		if setter, is := target.(timeoutSetter); is {
			setter.setTimeout(timeout)
		}
	})
}

func WithPacketPoolProvider(provider pool.PoolProvider[*InputPacket]) option.AnyOption {
	type packetPoolProviderSetter interface {
		setPacketPoolProvider(provider pool.PoolProvider[*InputPacket])
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(packetPoolProviderSetter); is {
			setter.setPacketPoolProvider(provider)
		}
	})
}

func WithReadBufferPool(bufferPool pool.BufferPool) option.AnyOption {
	type readBufferPoolSetter interface {
		setReadBufferPool(bufferPool pool.BufferPool)
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(readBufferPoolSetter); is {
			setter.setReadBufferPool(bufferPool)
		}
	})
}

func WithReadBufferPoolProvider(provider func() pool.BufferPool) option.AnyOption {
	type readBufferPoolProviderSetter interface {
		setReadBufferPoolProvider(provider func() pool.BufferPool)
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(readBufferPoolProviderSetter); is {
			setter.setReadBufferPoolProvider(provider)
		}
	})
}

func WithKeepChooser(chooser KeepChooser) option.AnyOption {
	return option.AnyCustom(func(target any) {
		if setter, is := target.(keepChooserSetter); is {
			setter.setKeepChooser(chooser)
		}
	})
}

func WithChannelActionHandler(handler ChannelActionHandler) option.AnyOption {
	type channelActionHandlerSetter interface {
		setChannelActionHandler(handler ChannelActionHandler)
	}
	return option.AnyCustom(func(target any) {
		if setter, is := target.(channelActionHandlerSetter); is {
			setter.setChannelActionHandler(handler)
		}
	})
}

func WithContext(ctx any) option.AnyOption {
	return option.AnyCustom(func(target any) {
		if setter, is := target.(contextSetter); is {
			setter.setContext(ctx)
		}
	})
}

type StreamOption option.CustomOption[Stream]

func WithStreamTimeout(timeout time.Duration) StreamOption {
	if timeout != 0 && timeout < time.Second {
		timeout = time.Second
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(timeoutSetter); is {
			setter.setTimeout(timeout)
		}
	})
}

func WithPacketHandler(handler PacketHandler) StreamOption {
	type packetHandlerSetter interface {
		setPacketHandler(handler PacketHandler)
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(packetHandlerSetter); is {
			setter.setPacketHandler(handler)
		}
	})
}

func WithStreamKeepChooser(chooser KeepChooser) StreamOption {
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(keepChooserSetter); is {
			setter.setKeepChooser(chooser)
		}
	})
}

func WithOnParseError(handler ParseErrorHandler) StreamOption {
	type onParseErrorSetter interface {
		setOnParseError(handler ParseErrorHandler)
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(onParseErrorSetter); is {
			setter.setOnParseError(handler)
		}
	})
}

func WithOnLossPacket(handler LossPacketHandler) StreamOption {
	type onLossPacketSetter interface {
		setOnLossPacket(handler LossPacketHandler)
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(onLossPacketSetter); is {
			setter.setOnLossPacket(handler)
		}
	})
}

func WithOnTimeout(handler StreamTimeoutHandler) StreamOption {
	type onTimeoutSetter interface {
		setOnTimeout(handler StreamTimeoutHandler)
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(onTimeoutSetter); is {
			setter.setOnTimeout(handler)
		}
	})
}

func WithOnClose(handler StreamCloseHandler) StreamOption {
	type onCloseSetter interface {
		setOnClose(handler StreamCloseHandler)
	}
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(onCloseSetter); is {
			setter.setOnClose(handler)
		}
	})
}

func WithStreamContext(ctx any) StreamOption {
	return option.Custom[Stream](func(stream Stream) {
		if setter, is := stream.(contextSetter); is {
			setter.setContext(ctx)
		}
	})
}

type ServerManagerOption option.CustomOption[*ServerManager]

func WithServerOptions(options ...option.AnyOption) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		m.serverOptions = options
	})
}

func AppendServerOptions(options ...option.AnyOption) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		m.serverOptions = append(m.serverOptions, options...)
	})
}

func WithServerAllocMode(allocMode ServerAllocMode) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		m.allocMode = allocMode
	})
}

func WithServerMaxUsed(maxUsed int) ServerManagerOption {
	if maxUsed < 1 {
		maxUsed = 1
	}
	return option.Custom[*ServerManager](func(m *ServerManager) {
		m.maxUsed = maxUsed
	})
}

func WithAllocMaxRetry(maxRetry int) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		m.allocMaxRetry = maxRetry
	})
}

type Port struct {
	RTP  int
	RTCP int
}

func WithPortRange(start int, end int, excludes ...int) ServerManagerOption {
	excludeSet := make(map[int]struct{}, len(excludes))
	for _, exclude := range excludes {
		excludeSet[exclude] = struct{}{}
	}
	return option.Custom[*ServerManager](func(m *ServerManager) {
		for i := start; i < end; i += 2 {
			var ins [4]bool
			_, ins[0] = excludeSet[i]
			_, ins[1] = excludeSet[i+1]
			_, ins[2] = m.portSet[i]
			_, ins[3] = m.portSet[i+1]
			if ins != [4]bool{} {
				continue
			}
			m.portSet[i], m.portSet[i+1] = struct{}{}, struct{}{}
			m.allocInfos = append(m.allocInfos, serverAllocInfo{
				rtpPort:  i,
				rtcpPort: i + 1,
			})
		}
	})
}

func WithPort(rtp, rtcp int) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		var in [2]bool
		_, in[0] = m.portSet[rtp]
		_, in[1] = m.portSet[rtcp]
		if in == [2]bool{} {
			m.portSet[rtp], m.portSet[rtcp] = struct{}{}, struct{}{}
			m.allocInfos = append(m.allocInfos, serverAllocInfo{
				rtpPort:  rtp,
				rtcpPort: rtcp,
			})
		}
	})
}

func WithPorts(ports ...Port) ServerManagerOption {
	return option.Custom[*ServerManager](func(m *ServerManager) {
		for _, port := range ports {
			var ins [2]bool
			_, ins[0] = m.portSet[port.RTP]
			_, ins[1] = m.portSet[port.RTCP]
			if ins == [2]bool{} {
				m.portSet[port.RTP], m.portSet[port.RTCP] = struct{}{}, struct{}{}
				m.allocInfos = append(m.allocInfos, serverAllocInfo{
					rtpPort:  port.RTP,
					rtcpPort: port.RTCP,
				})
			}
		}
	})
}
