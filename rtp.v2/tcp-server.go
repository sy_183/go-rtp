package rtp

import (
	"errors"
	"gitee.com/sy_183/go-common/container"
	"gitee.com/sy_183/go-common/lifecycle.v2"
	"gitee.com/sy_183/go-common/lifecycle.v2/future"
	"gitee.com/sy_183/go-common/option"
	"gitee.com/sy_183/go-common/pool"
	"net"
	"sync/atomic"
	"time"
)

type TCPServer struct {
	lifecycle.Lifecycle
	interrupted atomic.Bool
	accepted    atomic.Bool

	addr     *net.TCPAddr
	listener atomic.Pointer[net.TCPListener]
	channels container.SyncMap[string, *TCPPassiveChannel]
	streams  container.SyncMap[string, *TCPStream]

	// 默认的通道配置项
	channelOptions []option.AnyOption

	serverActionHandler ServerActionHandler
}

func NewTCPServer(addr *net.TCPAddr, options ...option.AnyOption) *TCPServer {
	s := &TCPServer{
		addr: addr,
	}

	for _, opt := range options {
		opt.Apply(s)
	}

	if s.addr == nil {
		s.addr = &net.TCPAddr{IP: net.IP{0, 0, 0, 0}, Port: 5004}
	}
	s.Lifecycle = lifecycle.NewWithFunc(s.start, s.run, s.close)
	return s
}

func (s *TCPServer) setServerActionHandler(handler ServerActionHandler) {
	s.serverActionHandler = handler
}

func (s *TCPServer) setChannelOptions(options ...option.AnyOption) {
	s.channelOptions = options
}

func (s *TCPServer) appendChannelOptions(options ...option.AnyOption) {
	s.channelOptions = append(s.channelOptions, options...)
}

func (s *TCPServer) Addr() net.Addr {
	return s.addr
}

func (s *TCPServer) serverAction(action Action, err error, args ...any) {
	if s.serverActionHandler != nil {
		s.serverActionHandler(s, action, err, args...)
	}
}

func (s *TCPServer) newChannel(id string, conn *net.TCPConn, options ...option.AnyOption) *TCPPassiveChannel {
	ch := &TCPPassiveChannel{
		id:   id,
		conn: conn,
	}
	ch.server.Store(s)

	for _, opt := range options {
		opt.Apply(ch)
	}

	if ch.readBufferPool == nil {
		ch.readBufferPool = pool.NewDefaultBufferPool(DefaultBufferPoolSize, DefaultBufferReverse, pool.ProvideSyncPool[*pool.Buffer])
	}
	if ch.packetPool == nil {
		ch.setPacketPoolProvider(pool.ProvideSyncPool[*InputPacket])
	}
	if ch.keepChooser == nil {
		ch.keepChooser = NewDefaultKeepChooser()
	}
	ch.Lifecycle = lifecycle.NewWithFunc(nil, ch.run, ch.close)
	return ch
}

func (s *TCPServer) accept(listener *net.TCPListener) (ok bool, err error) {
	// 获取最早过期的流及其过期时间
	var deadline time.Time
	var minExpireStream *TCPStream
	for _, stream := range s.streams.Values() {
		if d := stream.deadline(); d != zeroTime && (deadline == zeroTime || d.Before(deadline)) {
			minExpireStream = stream
			deadline = d
		}
	}

	if deadline == zeroTime {
		// 服务未添加任何流或所有流中都没有设置过期时间，使用最小过期时间
		deadline = time.Now().Add(time.Second)
	}
	if err := listener.SetDeadline(deadline); err != nil {
		s.serverAction(ActionSetDeadline, err, deadline)
	}

	conn, err := listener.AcceptTCP()
	if err != nil {
		if netErr, is := err.(net.Error); is && netErr.Timeout() {
			if minExpireStream != nil {
				// 最早过期的流超时，将流从服务中移除。此处可能出现与其他线程(例如流的Close方
				// 法)同时移除流的情况，因此需要判断流是否被其他线程移除
				if minExpireStream.server.CompareAndSwap(s, nil) {
					if stream, _ := s.streams.LoadAndDelete(minExpireStream.id); stream != nil {
						minExpireStream.doOnTimeout()
						minExpireStream.doOnClose()
					}
				}
			}
			return !s.interrupted.Load(), nil
		}
		if errors.Is(err, net.ErrClosed) {
			return false, nil
		}
		s.serverAction(ActionAccept, err)
		s.doClose(listener)
		return false, err
	}
	channelOptions := append([]option.AnyOption(nil), s.channelOptions...)
	s.serverAction(ActionAccept, err, conn, &channelOptions)

	// 获取通道ID并检查通道是否冲突，如果冲突，移除旧的通道
	id := conn.RemoteAddr().String()
	if old, _ := s.channels.LoadAndDelete(id); old != nil {
		// 将通道的TCP服务设为nil，防止通道关闭后将拥有同样ID的新通道从服务中移除
		if old.server.CompareAndSwap(s, nil) {
			old.Close()
		}
	}

	ch := s.newChannel(id, conn, channelOptions...)
	s.serverAction(ActionNewChannel, nil, ch)
	// 根据ID匹配流
	if stream, _ := s.streams.Load(id); stream != nil {
		// 设置流的通道后，流的关闭操作会将通道关闭。
		stream.channel.Store(ch)
		if stream.server.CompareAndSwap(s, nil) {
			// 成功将流的服务设置为空，将流从服务中移除并关联到通道上
			s.streams.Delete(id)
			ch.stream.Store(stream)
			if tAddr, is := ch.localAddr.(*net.TCPAddr); is {
				stream.localAddr.Store(tAddr)
			}
		} else {
			// 流的服务被其他线程(流的Close方法)设置为空，说明流已经被关闭，那么也会从服务中移
			// 除，并且不需要关联到通道上
		}
	}

	s.channels.Store(id, ch)
	ch.Future(lifecycle.FutureTypeClosed, future.CallbackFuture[error](func(err error) {
		// 此处判断通道是否已经被移除，防止将拥有同样ID的新通道从服务中移除
		if ch.server.CompareAndSwap(s, nil) {
			s.channels.Delete(ch.id)
		}
	}))
	ch.Start()
	return true, nil
}

func (s *TCPServer) done() {
	s.accepted.Store(false)
	s.streams.Range(func(id string, stream *TCPStream) bool {
		// 由于设置了accepted标志，此处可能出现与Stream方法或流的关闭同时移除流的情况，因此需
		// 要判断流是否被其他线程移除
		if stream.server.CompareAndSwap(s, nil) {
			if stream, exist := s.streams.LoadAndDelete(id); exist {
				stream.doOnClose()
			}
		}
		return true
	})
}

func (s *TCPServer) start(lifecycle.Lifecycle) (err error) {
	defer func() {
		if err != nil {
			s.done()
		}
	}()
	listener := s.listener.Load()
	if listener == nil {
		listener, err = net.ListenTCP("tcp", s.addr)
		if err != nil {
			s.serverAction(ActionListen, err)
			return err
		}
		s.listener.Store(listener)
		s.serverAction(ActionListen, nil)
		if s.interrupted.Load() {
			s.doClose(listener)
			return lifecycle.ErrInterrupted
		}
	}
	now := time.Now()
	s.streams.Range(func(id string, stream *TCPStream) bool {
		stream.lastTime = now
		return true
	})
	return nil
}

func (s *TCPServer) run(lifecycle.Lifecycle) error {
	defer s.done()
	listener := s.listener.Load()
	for {
		if ok, err := s.accept(listener); !ok {
			return err
		}
	}
}

func (s *TCPServer) doClose(listener *net.TCPListener) error {
	err := listener.Close()
	if err != nil {
		if errors.Is(err, net.ErrClosed) {
			return nil
		}
	}
	s.serverAction(ActionClose, err)
	return err
}

func (s *TCPServer) close(lifecycle.Lifecycle) error {
	if s.interrupted.CompareAndSwap(false, true) {
		if listener := s.listener.Load(); listener != nil {
			return s.doClose(listener)
		}
	}
	return nil
}

func (s *TCPServer) Stream(remoteAddr net.Addr, ssrc int64, options ...StreamOption) (Stream, error) {
	id := remoteAddr.String()
	if stream, exist := s.streams.Load(id); exist {
		return stream, ErrStreamExist
	}
	stream := &TCPStream{}
	stream.localAddr.Store(s.addr)
	stream.SetRemoteAddr(remoteAddr)
	stream.SetSSRC(ssrc)
	stream.lastTime = time.Now()

	for _, opt := range options {
		opt.Apply(stream)
	}

	// 尝试匹配通道
	if ch, _ := s.channels.Load(id); ch != nil {
		// 先判断一遍通道是否已经匹配了流
		if stream := ch.stream.Load(); stream != nil {
			return stream, ErrStreamExist
		}
		// 尝试将通道匹配流，如果匹配失败则说明已经被其他的流匹配
		if !ch.stream.CompareAndSwap(nil, stream) {
			return ch.stream.Load(), ErrStreamExist
		}
		stream.channel.Store(ch)
		if tAddr, is := ch.localAddr.(*net.TCPAddr); is {
			stream.localAddr.Store(tAddr)
		}
		// 通道匹配成功，配置通道关闭后关闭流的回调
		ch.Future(lifecycle.FutureTypeClosed, future.CallbackFuture[error](func(err error) {
			stream.doOnClose()
		}))
		return stream, nil
	}

	// 没有找到匹配的通道，先添加到服务中，等待新通道匹配流
	stream.server.Store(s)
	if stream, exist := s.streams.LoadOrStore(id, stream); exist {
		return stream, ErrStreamExist
	}

	if s.accepted.Load() {
		// 由于服务已经不在接收新连接，需要移除刚刚添加的流，此处可能出现与其他线程(例如主线程最
		// 结束时清除流)同时移除流的情况，因此需要判断流是否被主线程移除
		if stream.server.CompareAndSwap(s, nil) {
			if stream, _ := s.streams.LoadAndDelete(id); stream != nil {
				stream.doOnClose()
			}
		}
	}

	return stream, nil
}
