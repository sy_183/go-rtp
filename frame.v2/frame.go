package frame

import (
	"bytes"
	"encoding/binary"
	"gitee.com/sy_183/go-common/pool"
	ioUtil "gitee.com/sy_183/go-common/utils/io"
	"gitee.com/sy_183/go-rtp/rtp.v2"
	"io"
	"net"
	"time"
)

type FrameWriter interface {
	Frame() FrameI

	SetFrame(frame FrameI)

	Size() int

	WriteTo(w io.Writer) (n int64, err error)

	Read(p []byte) (n int, err error)
}

type abstractFrameWriter struct {
	frame FrameI
	size  int
}

func (f *abstractFrameWriter) Frame() FrameI {
	return f.frame
}

func (f *abstractFrameWriter) SetFrame(frame FrameI) {
	f.frame = frame
	f.size = 0
}

type FullFrameWriter struct {
	abstractFrameWriter
	packet rtp.Packet

	timestamp       uint32
	timestampEnable bool

	payloadType       uint8
	payloadTypeEnable bool

	sequenceNumber       uint16
	sequenceNumberEnable bool

	ssrc       uint32
	ssrcEnable bool
}

func NewFullFrameWriter(frame FrameI) *FullFrameWriter {
	return &FullFrameWriter{abstractFrameWriter: abstractFrameWriter{frame: frame}}
}

func (f *FullFrameWriter) Timestamp() (uint32, bool) {
	if f.timestampEnable {
		return f.timestamp, true
	}
	return 0, false
}

func (f *FullFrameWriter) PayloadType() (uint8, bool) {
	if f.payloadTypeEnable {
		return f.payloadType, true
	}
	return 0, false
}

func (f *FullFrameWriter) SequenceNumber() (uint16, bool) {
	if f.sequenceNumberEnable {
		return f.sequenceNumber, true
	}
	return 0, false
}

func (f *FullFrameWriter) SSRC() (uint32, bool) {
	if f.ssrcEnable {
		return f.ssrc, true
	}
	return 0, false
}

func (f *FullFrameWriter) SetTimestamp(timestamp uint32) {
	f.timestamp = timestamp
	f.timestampEnable = true
}

func (f *FullFrameWriter) SetPayloadType(payloadType uint8) {
	if payloadType < 128 {
		f.payloadType = payloadType
		f.payloadTypeEnable = true
	}
}

func (f *FullFrameWriter) SetSequenceNumber(sequenceNumber uint16) {
	f.sequenceNumber = sequenceNumber
	f.sequenceNumberEnable = true
}

func (f *FullFrameWriter) SetSSRC(ssrc uint32) {
	f.ssrc = ssrc
	f.ssrcEnable = true
}

func (f *FullFrameWriter) DisableTimestamp() {
	f.timestamp = 0
	f.timestampEnable = false
}

func (f *FullFrameWriter) DisablePayloadType() {
	f.payloadType = 0
	f.payloadTypeEnable = false
}

func (f *FullFrameWriter) DisableSequenceNumber() {
	f.sequenceNumber = 0
	f.sequenceNumberEnable = false
}

func (f *FullFrameWriter) DisableSSRC() {
	f.ssrc = 0
	f.ssrcEnable = false
}

func (f *FullFrameWriter) Size() int {
	if f.size != 0 {
		return f.size
	}
	var s int
	f.frame.Range(func(i int, packet rtp.PacketI) bool {
		s += packet.Size() + 2
		return true
	})
	f.size = s
	return s
}

func (f *FullFrameWriter) detachPacket(packetP *rtp.PacketI, detached *bool) {
	if !*detached {
		packet := *packetP
		switch p := packet.(type) {
		case *rtp.Packet:
			f.packet.Header = p.Header
			f.packet.SetPayload(packet.Payload())
			f.packet.SetPaddingLength(packet.PaddingLength())
		case *rtp.InputPacket:
			f.packet.Header = p.Header
			f.packet.SetPayload(packet.Payload())
			f.packet.SetPaddingLength(packet.PaddingLength())
		default:
			f.packet.Init(packet.Payload())
			f.packet.SetMarker(packet.Marker())
			f.packet.SetPayloadType(packet.PayloadType())
			f.packet.SetSequenceNumber(packet.SequenceNumber())
			f.packet.SetTimestamp(packet.Timestamp())
			f.packet.SetSSRC(packet.SSRC())
			f.packet.CSRCs().Replace(*packet.CSRCs()...)
			if f.packet.SetExtension(packet.HasExtension()); packet.HasExtension() {
				src, dst := packet.Extension(), f.packet.Extension()
				dst.SetProfile(src.Profile())
				dst.Replace(src.Contents()...)
			}
			f.packet.SetPaddingLength(packet.PaddingLength())
		}
		*detached = true
		*packetP = &f.packet
	}
}

func (f *FullFrameWriter) modifyPacket(packetP *rtp.PacketI) (modified bool) {
	packet := *packetP
	var detached bool
	if timestamp, ok := f.Timestamp(); ok {
		f.detachPacket(&packet, &detached)
		packet.SetTimestamp(timestamp)
	}
	if payloadType, ok := f.PayloadType(); ok {
		f.detachPacket(&packet, &detached)
		packet.SetPayloadType(payloadType)
	}
	if sequenceNumber, ok := f.SequenceNumber(); ok {
		f.detachPacket(&packet, &detached)
		packet.SetSequenceNumber(sequenceNumber)
		f.sequenceNumber++
	}
	if ssrc, ok := f.SSRC(); ok {
		f.detachPacket(&packet, &detached)
		packet.SetSSRC(ssrc)
	}
	*packetP = packet
	return detached
}

func (f *FullFrameWriter) WriteTo(w io.Writer) (n int64, err error) {
	var buf [2]byte
	f.frame.Range(func(i int, packet rtp.PacketI) bool {
		f.modifyPacket(&packet)
		binary.BigEndian.PutUint16(buf[:], uint16(packet.Size()))
		if err = ioUtil.Write(w, buf[:], &n); err != nil {
			return false
		}
		if err = ioUtil.WriteTo(packet, w, &n); err != nil {
			return false
		}
		return true
	})
	return
}

func (f *FullFrameWriter) WriteToUDP(buffer *bytes.Buffer, udpConn *net.UDPConn) (err error) {
	f.frame.Range(func(i int, packet rtp.PacketI) bool {
		defer buffer.Reset()
		f.modifyPacket(&packet)
		packet.WriteTo(buffer)
		if _, err = udpConn.Write(buffer.Bytes()); err != nil {
			return false
		}
		return true
	})
	return
}

func (f *FullFrameWriter) Read(p []byte) (n int, err error) {
	w := ioUtil.Writer{Buf: p}
	n64, _ := f.WriteTo(&w)
	return int(n64), io.EOF
}

type PayloadFrameWriter struct {
	abstractFrameWriter
}

func NewPayloadFrameWriter(frame FrameI) FrameWriter {
	return &PayloadFrameWriter{abstractFrameWriter{frame: frame}}
}

func (f *PayloadFrameWriter) Size() int {
	if f.size != 0 {
		return f.size
	}
	var s int
	f.frame.Range(func(i int, packet rtp.PacketI) bool {
		s += packet.Payload().Size()
		return true
	})
	f.size = s
	return s
}

func (f *PayloadFrameWriter) WriteTo(w io.Writer) (n int64, err error) {
	f.frame.Range(func(i int, packet rtp.PacketI) bool {
		if err = ioUtil.WriteTo(packet.Payload(), w, &n); err != nil {
			return false
		}
		return true
	})
	return
}

func (f *PayloadFrameWriter) Read(p []byte) (n int, err error) {
	w := ioUtil.Writer{Buf: p}
	n64, _ := f.WriteTo(&w)
	return int(n64), io.EOF
}

type FrameI interface {
	Len() int

	Range(f func(i int, packet rtp.PacketI) bool) bool

	Packet(i int) rtp.PacketI

	Timestamp() uint32

	SetTimestamp(timestamp uint32)

	PayloadType() uint8

	SetPayloadType(typ uint8)

	Append(packet rtp.PacketI) bool

	AddRelation(relation pool.Reference)

	Clear()

	pool.Reference
}

func UseFrame(frame FrameI) FrameI {
	frame.AddRef()
	return frame
}

type InputFrame struct {
	packets     []*rtp.InputPacket
	timestamp   uint32
	payloadType uint8
	start       time.Time
	end         time.Time
	relations   pool.Relations
	pool        pool.Pool[*InputFrame]
}

func NewInputFrame() FrameI {
	return new(InputFrame)
}

func timeRangeExpand(start, end *time.Time, cur time.Time, timeNodeLen int) {
	if start.After(*end) {
		panic("start time after end time")
	}
	if start.IsZero() {
		*start = cur
		*end = cur
	} else {
		du := cur.Sub(*end)
		if du < 0 {
			guess := time.Duration(0)
			if timeNodeLen > 2 {
				guess = (end.Sub(*start)) / time.Duration(timeNodeLen-2)
			}
			offset := du - guess
			*start = start.Add(offset)
		}
		*end = cur
	}
}

func (f *InputFrame) Len() int {
	return len(f.packets)
}

func (f *InputFrame) Range(fn func(i int, packet rtp.PacketI) bool) bool {
	for i, packet := range f.packets {
		if !fn(i, packet) {
			return false
		}
	}
	return true
}

func (f *InputFrame) Packet(i int) rtp.PacketI {
	return f.packets[i]
}

func (f *InputFrame) Timestamp() uint32 {
	return f.timestamp
}

func (f *InputFrame) SetTimestamp(timestamp uint32) {
	for _, packet := range f.packets {
		packet.SetTimestamp(timestamp)
	}
	f.timestamp = timestamp
}

func (f *InputFrame) PayloadType() uint8 {
	return f.payloadType
}

func (f *InputFrame) SetPayloadType(typ uint8) {
	for _, packet := range f.packets {
		packet.SetPayloadType(typ)
	}
	f.payloadType = typ
}

func (f *InputFrame) Start() time.Time {
	return f.start
}

func (f *InputFrame) End() time.Time {
	return f.end
}

func (f *InputFrame) Append(packet rtp.PacketI) bool {
	if len(f.packets) == 0 {
		f.timestamp = packet.Timestamp()
		f.payloadType = packet.PayloadType()
	} else if f.timestamp != packet.Timestamp() || f.payloadType != packet.PayloadType() {
		return false
	}
	icPacket := packet.(*rtp.InputPacket)
	f.packets = append(f.packets, icPacket)
	timeRangeExpand(&f.start, &f.end, icPacket.Time(), len(f.packets))
	return true
}

var zeroTime = time.Time{}

func (f *InputFrame) AddRelation(relation pool.Reference) {
	f.relations.AddRelation(relation)
}

func (f *InputFrame) Clear() {
	for _, packet := range f.packets {
		packet.Release()
	}
	f.packets = f.packets[:0]
	f.timestamp = 0
	f.payloadType = 0
	f.start, f.end = zeroTime, zeroTime
}

func (f *InputFrame) Release() bool {
	if f.relations.Release() {
		f.Clear()
		if f.pool != nil {
			f.pool.Put(f)
		}
		return true
	}
	return false
}

func (f *InputFrame) AddRef() {
	f.relations.AddRef()
}

func (f *InputFrame) Use() *InputFrame {
	f.AddRef()
	return f
}

type Frame struct {
	packets     []rtp.PacketI
	timestamp   uint32
	payloadType uint8
	relations   pool.Relations
	pool        pool.Pool[*Frame]
}

func NewFrame(pool pool.Pool[*Frame]) *Frame {
	return &Frame{pool: pool}
}

func (f *Frame) Len() int {
	return len(f.packets)
}

func (f *Frame) Range(fn func(i int, packet rtp.PacketI) bool) bool {
	for i, packet := range f.packets {
		if !fn(i, packet) {
			return false
		}
	}
	return true
}

func (f *Frame) Packet(i int) rtp.PacketI {
	return f.packets[i]
}

func (f *Frame) Timestamp() uint32 {
	return f.timestamp
}

func (f *Frame) SetTimestamp(timestamp uint32) {
	for _, packet := range f.packets {
		packet.SetTimestamp(timestamp)
	}
	f.timestamp = timestamp
}

func (f *Frame) PayloadType() uint8 {
	return f.payloadType
}

func (f *Frame) SetPayloadType(typ uint8) {
	for _, packet := range f.packets {
		packet.SetPayloadType(typ)
	}
	f.payloadType = typ
}

func (f *Frame) Append(packet rtp.PacketI) bool {
	if len(f.packets) == 0 {
		f.timestamp = packet.Timestamp()
		f.payloadType = packet.PayloadType()
	} else if f.timestamp != packet.Timestamp() || f.payloadType != packet.PayloadType() {
		return false
	}
	f.packets = append(f.packets, packet)
	return true
}

func (f *Frame) AddRelation(relation pool.Reference) {
	f.relations.AddRelation(relation)
}

func (f *Frame) Clear() {
	for _, packet := range f.packets {
		packet.Release()
	}
	f.packets = f.packets[:0]
	f.timestamp = 0
	f.payloadType = 0
}

func (f *Frame) AddRef() {
	f.relations.AddRef()
}

func (f *Frame) Release() bool {
	if f.relations.Release() {
		f.Clear()
		if f.pool != nil {
			f.pool.Put(f)
		}
		return true
	}
	return false
}

func (f *Frame) Use() *Frame {
	f.AddRef()
	return f
}
