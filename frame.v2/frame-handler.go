package frame

import (
	"gitee.com/sy_183/go-common/pool"
	"gitee.com/sy_183/go-rtp/rtp.v2"
)

type FrameHandler interface {
	HandleFrame(stream rtp.Stream, frame *InputFrame)

	OnStreamClosed(stream rtp.Stream)
}

type FrameHandlerFunc struct {
	HandleFrameFn    func(stream rtp.Stream, frame *InputFrame)
	OnStreamClosedFn func(stream rtp.Stream)
}

func (h FrameHandlerFunc) HandleFrame(stream rtp.Stream, frame *InputFrame) {
	if handleFrameFn := h.HandleFrameFn; handleFrameFn != nil {
		handleFrameFn(stream, frame)
	}
}

func (h FrameHandlerFunc) OnStreamClosed(stream rtp.Stream) {
	if onStreamClosedFn := h.OnStreamClosedFn; onStreamClosedFn != nil {
		onStreamClosedFn(stream)
	}
}

type FrameRTPHandler struct {
	cur          *InputFrame
	frameHandler FrameHandler
	framePool    *pool.SyncPool[*InputFrame]
}

func newFrame(p *pool.SyncPool[*InputFrame]) *InputFrame {
	return &InputFrame{pool: p}
}

func NewFrameRTPHandler(frameHandler FrameHandler) *FrameRTPHandler {
	return &FrameRTPHandler{
		frameHandler: frameHandler,
		framePool:    pool.NewSyncPool(newFrame),
	}
}

func (h *FrameRTPHandler) HandlePacket(stream rtp.Stream, packet *rtp.InputPacket) error {
	if h.cur != nil && (packet.Timestamp() != h.cur.Timestamp() || packet.PayloadType() != h.cur.PayloadType()) {
		h.frameHandler.HandleFrame(stream, h.cur)
		h.cur = nil
	}
	if h.cur == nil {
		h.cur = h.framePool.Get().Use()
	}
	h.cur.Append(packet)
	return nil
}

func (h *FrameRTPHandler) OnStreamClosed(stream rtp.Stream) {
	if h.cur != nil && h.cur.Len() != 0 {
		h.frameHandler.HandleFrame(stream, h.cur)
		h.cur = nil
	}
	h.frameHandler.OnStreamClosed(stream)
}
