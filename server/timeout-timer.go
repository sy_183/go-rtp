package server

import (
	"gitee.com/sy_183/go-common/lock"
	"sync"
	"sync/atomic"
	timePkg "time"
)

// TimeoutManager 超时时间管理器
type TimeoutManager struct {
	time      timePkg.Time
	timeout   atomic.Int64
	timer     *timePkg.Timer
	timerLock sync.Mutex
}

// Time 获取上一次设置的时间
func (m *TimeoutManager) Time() timePkg.Time {
	return m.time
}

// SetTime 更新当前时间，更新后调用Deadline方法可获取基于更新时间的超时期限，此方法不更新计时器的状态，
// 一般用于网络IO的超时管理
func (m *TimeoutManager) SetTime(time timePkg.Time) {
	m.time = time
}

// Timeout 获取当前设置的超时时间
func (m *TimeoutManager) Timeout() timePkg.Duration {
	return timePkg.Duration(m.timeout.Load())
}

// SetTimeout 设置超时时间(设置为0永不超时)，设置后调用Deadline方法可获取基于此超时时间的超时期限，此
// 方法不更新计时器的状态，一般用于网络IO的超时管理
func (m *TimeoutManager) SetTimeout(timeout timePkg.Duration) {
	m.timeout.Store(int64(timeout))
}

// Deadline 获取基于设置时间的超时期限，如果超时时间为0(永不超时)，则返回时间的零值
func (m *TimeoutManager) Deadline() timePkg.Time {
	if timeout := m.Timeout(); timeout > 0 {
		return m.time.Add(timeout)
	}
	return timePkg.Time{}
}

// UpdateTime 与 SetTime 类似，但如果计时器在工作，则会重置计时器的超时时间
func (m *TimeoutManager) UpdateTime(time timePkg.Time) {
	lock.LockDo(&m.timerLock, func() {
		m.SetTime(time)
		if m.timer != nil {
			deadline := m.time.Add(m.Timeout())
			if now := timePkg.Now(); deadline.After(now) {
				m.timer.Reset(deadline.Sub(now))
			} else {
				m.timer.Reset(0)
			}
		}
	})
}

// UpdateTimeout 与 SetTimeout 方法类似，但如果计时器在工作，则会更新计时器状态。如果需要更新计时器状
// 态，那么当设置为0时，关闭计时器；否则重置计时器的超时时间
func (m *TimeoutManager) UpdateTimeout(timeout timePkg.Duration) {
	lock.LockDo(&m.timerLock, func() {
		m.SetTimeout(timeout)
		if m.timer != nil {
			if timeout <= 0 {
				m.timer.Stop()
				m.timer = nil
			} else {
				deadline := m.time.Add(timeout)
				if now := timePkg.Now(); deadline.After(now) {
					m.timer.Reset(deadline.Sub(now))
				} else {
					m.timer.Reset(0)
				}
			}
		}
	})
}

// Init 如果计时器已关闭，初始化计时器的超时回调函数
func (m *TimeoutManager) Init(f func()) {
	lock.LockDo(&m.timerLock, func() {
		if m.timer != nil {
			return
		}
		m.SetTime(timePkg.Now())
		if timeout := m.Timeout(); timeout > 0 {
			m.timer = timePkg.AfterFunc(timeout, func() {
				f()
				m.Close()
			})
		}
	})
}

func (m *TimeoutManager) Close() {
	lock.LockDo(&m.timerLock, func() {
		if m.timer != nil {
			m.timer.Stop()
			m.timer = nil
		}
	})
}
